function [nm, nm_f, nm_w, nm_n] = normal2(N, ruido)
    t0 = 0:0.0001:3/60;
    t2 = 0:0.0001:57/60;
    
    %Inicializando vari�veis de retorno
    nm = cell(1);
    nm_w = cell(1);
    nm_f = cell(1);
    nm_n = cell(1);
    
    %Par�metros do filtro notch
    wo = 60/(10*size(t0,2)); bw=wo/35;
    [b, a] = iirnotch(wo, bw);

    for j = 1:N
        %Frequ�ncia da senoide
        freq = round(-2+rand(1)*4)+60;
        amp = round(-5+rand(1)*10)*0.01+1;
        
        %Vari�vel com sen�ide pura
        aux = amp*sin(2*pi*t0*freq);
        if (ruido ~= 0)
            aux = add_awgn_noise(aux, ruido);
        end
        nm{j,1} = aux;
        
        %Gerando sinal com notch
        hf = filter(b, a, [sin(2*pi*t2*60) aux]);
        nm_n{j,1} = hf(:,size(t2,2)+1:size(hf,2));
        
        %Gerando sinal com FFT
        Y = fft(aux);
        L = size(t0,2);
        P2 = abs(Y/L);
        P1 = P2(1:round(L/2)+1);
        P1(2:end-1) = 2*P1(2:end-1);
        
        nm_f{j,1} = P1;
        
        %Gerando sinal com Wavelet
        [C, L] =  wavedec(aux, 4, 'db4');
        d1 = appcoef(C, L, 'db4', 1);
        
        nm_w{j,1} = d1;
    end
    %Transformando em matriz
    nm = cell2mat(nm);
    nm_w = cell2mat(nm_w);
    nm_f = cell2mat(nm_f);
    nm_n = cell2mat(nm_n);
end
    
            