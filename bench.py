import numpy as np
from Bucket import Bucket

quant = 50

b = Bucket(200,0,freq=60)
b1 = Bucket(200,10,freq=60)
b2 = Bucket(200,20,freq=60)
b3 = Bucket(200,30,freq=60)
b4 = Bucket(200,50,freq=60)

for i in range(quant):
    bc_0 = b.createBucket()
    bc_10 = b1.createBucket()
    bc_20 = b2.createBucket()
    bc_30 = b3.createBucket()
    bc_50 = b4.createBucket()

    np.save('bench/bc0_'+str(i), bc_0)
    np.save('bench/bc10_'+str(i), bc_10)
    np.save('bench/bc20_'+str(i), bc_20)
    np.save('bench/bc30_'+str(i), bc_30)
    np.save('bench/bc50_'+str(i), bc_50)