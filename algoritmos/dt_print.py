from sklearn.tree import DecisionTreeClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
from sklearn import tree
import graphviz

import numpy as np

X = np.load('dados.npy');
y = np.load('targetSVM.npy');
classes=['sn', 'i', 'sg', 'sw', 'osc', 'flk', 'hm', 'ntc', 'spk', 'dcl']

# define os dados de treino e os dados de teste 70% e 30%
X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)

#Cria o Pipeline
classifier = Pipeline([('scl', StandardScaler()),
        ('clf',DecisionTreeClassifier(presort=True, criterion='entropy',
                                      max_features='log2', random_state=0,
                                      splitter='best'))])

#Target esta errado, para o svm o target deve conter apenas uma unica coluna;
classifier = classifier.fit(X_train, y_train)

dt = classifier.named_steps['clf']

dot_data = tree.export_graphviz(dt, out_file=None, class_names=classes,filled=True, rounded=True,  
                         special_characters=True)
graph = graphviz.Source(dot_data) 
graph.render('arvore')
