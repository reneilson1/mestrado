from sklearn.grid_search import GridSearchCV
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn import datasets
from sklearn.cross_validation import train_test_split
from sklearn.decomposition import PCA

import numpy as np
from sklearn.externals import joblib

X = np.load('dados0.npy');
y = np.load('target0.npy');

#indices = np.load('indices.npy')

#X = X[:,indices[0:12]]


# define os dados de treino e os dados de teste 70% e 30%
X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)


pipe_svc = Pipeline([('scl', StandardScaler()),
            ('clf',MLPClassifier(random_state=1, max_iter=1000))])

#param_solver = ['lbfgs', 'sgd', 'adam']
#param_ativacao = ['identity', 'logistic', 'tanh', 'relu']
#param_alpha = [1e+0, 1e-1, 1e-2, 1e-3]
#param_layer = [(2,2),(5,2),(10,2),(15,2)]
param_solver = ['lbfgs']
param_ativacao = ['relu']
param_alpha = [1e-1]
param_layer = [(2),(5),(10),(15),(10,2)]

param_grid = [{'clf__solver': param_solver,
               'clf__activation': param_ativacao,
               'clf__alpha': param_alpha,
               'clf__hidden_layer_sizes': param_layer}]

gs = GridSearchCV(estimator=pipe_svc, 
                  param_grid=param_grid, 
                  scoring='accuracy', 
                  cv=10,
                  n_jobs=1)

gs = gs.fit(X_train, y_train)

print(gs.best_params_)

clf = gs.best_estimator_

joblib.dump(clf, 'MLP.pkl')

print(clf.score(X_train, y_train))
print(clf.score(X_test, y_test))