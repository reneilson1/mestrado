# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 23:24:19 2017

@author: Joel Alves
"""

from sklearn.grid_search import GridSearchCV
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split

import numpy as np
from sklearn.externals import joblib

X = np.load('dados0.npy');
y = np.load('target0.npy');

# define os dados de treino e os dados de teste 70% e 30%
X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)
   
    
pipe_svc = Pipeline([('scl', StandardScaler()),
            ('clf', SVC(random_state=1))])

param_range = [1e+6, 1e-1]

param_grid = [{'clf__C': param_range,
               'clf__decision_function_shape': ['ovo', 'ovr'],
               'clf__kernel': ['rbf', 'linear', 'poly', 'sigmoid']}]

gs = GridSearchCV(estimator=pipe_svc, 
                  param_grid=param_grid, 
                  scoring='accuracy', 
                  cv=10,
                  n_jobs=1)


#Target esta errado, para o svm o target deve conter apenas uma unica coluna;
gs = gs.fit(X_train, y_train)

clf = gs.best_estimator_

joblib.dump(clf, 'SVM.pkl')

print(clf.score(X_train, y_train))
print(clf.score(X_test, y_test))