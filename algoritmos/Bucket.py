# -*- coding: utf-8 -*-
"""
Created on Mon Nov 13 12:29:40 2017

@author: Rene
"""

from disturbios import Disturbios
import numpy as np

class Bucket(object):
    
    def __init__(self, n_ex):
        self.n_ex = n_ex;
        
    def createBucket(self):
        d = Disturbios(self.n_ex, 200, 3, 60);
        
        #Distúrbios Simples
        sn = d.seno_puro();
        i = d.interrupt();
        sg = d.sag();
        sw = d.swell();
        osc = d.oscilacao();
        flk = d.flicker();
        hm = d.harmonica();
        ntc = d.notching();
        spk = d.spike();
        dcl = d.dc_level();
        
        dist = np.concatenate((sn, i, sg, sw, osc, flk, hm, ntc, spk, dcl), axis=0);
        
        return dist;
    
    def createLabels(self):
        #Criando label (n_d representa o número de distúrbios)
        n_d = 10;
        lbl = np.zeros([self.n_ex*n_d, n_d]);
        
        for i in range(0,(self.n_ex*n_d)):
            lbl[i][i/self.n_ex] = 1;
        
        return lbl;
    
    def createLabelsSVM(self):
        #Criando label (n_d representa o número de distúrbios)
        n_d = 10;
        lbl = np.zeros([self.n_ex*n_d]);
        
        for i in range(0,(self.n_ex*n_d)):
            lbl[i]=i/self.n_ex;
            
        return lbl;
    
    def createFile(self, fileNameBucket, fileNameLabel, fileNameLabelSVM):
        np.save(fileNameBucket, self.createBucket());
        np.save(fileNameLabel, self.createLabels());
        np.save(fileNameLabelSVM, self.createLabelsSVM());
        
        