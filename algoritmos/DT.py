from sklearn.tree import DecisionTreeClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
#from sklearn import tree
#import graphviz
from sklearn.externals import joblib

import numpy as np

X = np.load('dados.npy');
y = np.load('target.npy');
classes=['no','yes']

# define os dados de treino e os dados de teste 70% e 30%
X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)

pipe_svc = Pipeline([('scl', StandardScaler()),
            ('clf',DecisionTreeClassifier(random_state=0))])

param_grid = [{'clf__criterion': ['entropy','gini'], 
                  'clf__max_features': ['sqrt','log2','auto'],
                  'clf__splitter': ['best', 'random'],
                  'clf__presort':[True, False]}]

gs = GridSearchCV(estimator=pipe_svc, 
                  param_grid=param_grid, 
                  scoring='accuracy', 
                  cv=10,
                  n_jobs=1)


gs = gs.fit(X_train, y_train)

clf = gs.best_estimator_

joblib.dump(clf, 'DT.pkl')

print(clf.get_params())

print(clf.score(X_test, y_test))

print(clf.score(X_train, y_train))

"""sc = StandardScaler()
sc.fit(X)
X = sc.transform(X)

clf = DecisionTreeClassifier(random_state=0)

clf.fit(X,y)

dt = pipe_svc.named_steps['clf']

dot_data = tree.export_graphviz(dt, out_file=None, class_names=classes,filled=True, rounded=True,  
                         special_characters=True)
graph = graphviz.Source(dot_data) 
graph.render('arvore')"""
