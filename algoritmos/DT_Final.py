from sklearn.tree import DecisionTreeClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.externals import joblib

import numpy as np

n=30
acuracy = np.zeros(n)
f1 = np.zeros(n)
class_names=['yes','no']

#Leitura de dados
X = np.load('dados0.npy')
y = np.load('target0.npy')
indices = np.load('indices.npy')

#X = X[:,indices[0:9]]
    
# define os dados de treino e os dados de teste 70% e 30%
X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)

#Cria o Pipeline
classifier = Pipeline([('scl', StandardScaler()),
        ('clf',DecisionTreeClassifier(presort=True, criterion='entropy',
                                      max_features='sqrt', random_state=0,
                                      splitter='best'))])

#Target esta errado, para o svm o target deve conter apenas uma unica coluna;
classifier = classifier.fit(X_train, y_train)
joblib.dump(classifier, 'DT.pkl')
print (classifier.score(X_train, y_train))
print (classifier.score(X_test, y_test))

for i in range(n):

    X = np.load('dados' + str(i+1) + '.npy')
    y = np.load('target' + str(i+1) + '.npy')
    
    #X = X[:, indices[0:9]]
    
    y_pred = classifier.predict(X)
    
    
    acuracy[i] = accuracy_score(y, y_pred)
    f1[i] = f1_score(y, y_pred, average='macro')
    
    #Imprime resultado
    print(classifier.score(X, y))
    print(f1[i])
    
    """
    # Compute confusion matrix
    cnf_matrix = confusion_matrix(y_test, y_pred)
    np.set_printoptions(precision=2)
    
    # Plot non-normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names,
                          title='Confusion matrix, without normalization')
    
    # Plot normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True,
                          title='Normalized confusion matrix')
    
    plt.show()"""  

np.save('Accuracy_DT', acuracy)
np.save('F1_DT', f1)
print(str(np.mean(acuracy)) + "+/-" + str(np.std(acuracy)))
print(str(np.mean(f1)) + "+/-" + str(np.std(f1)))