from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.externals import joblib

n=30
acuracy = np.zeros(n)
f1 = np.zeros(n)
class_names=['no', 'yes']

X = np.load('dados0.npy')
y = np.load('target0.npy')
indices = np.load('indices.npy')

#X = X[:,indices[0:9]]

# define os dados de treino e os dados de teste 70% e 30%
X_train, X_test, y_train, y_test = train_test_split(
     X, y, test_size=0.3, random_state=0)

#Cria o Pipeline
classifier = Pipeline([('scl', StandardScaler()),
            ('clf',RandomForestClassifier(n_estimators=10, criterion='entropy',
                                          max_features='sqrt', random_state=0))])
    
#Target esta errado, para o svm o target deve conter apenas uma unica coluna;
classifier = classifier.fit(X_train, y_train)

joblib.dump(classifier, 'RF.pkl')

y_pred = classifier.predict(X_test)
f = f1_score(y_test, y_pred, average='macro')
print(classifier.score(X_train, y_train))
print(f)

for i in range(n):
    
    X = np.load('dados' + str(i+1) + '.npy')
    y = np.load('target' + str(i+1) + '.npy')
    
    #X = X[:, indices[0:9]]
    
    y_pred = classifier.predict(X)
    
    
    acuracy[i] = accuracy_score(y, y_pred)
    f1[i] = f1_score(y, y_pred, average='macro')
    
    #Imprime resultado
    print(classifier.score(X, y))
    print(f1[i])

np.save('Accuracy_RF', acuracy)
np.save('F1_RF', f1)
print(str(np.mean(acuracy)) + "+/-" + str(np.std(acuracy)))
print(str(np.mean(f1)) + "+/-" + str(np.std(f1)))