# -*- coding: utf-8 -*-
"""
Created on Wed Nov  1 23:40:14 2017

@author: joeli
"""

from sklearn.grid_search import GridSearchCV
#from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

#from sklearn import datasets
from sklearn.cross_validation import train_test_split

import numpy as np
from sklearn.externals import joblib

X = np.load('dados0.npy');
y = np.load('target0.npy');

# define os dados de treino e os dados de teste 70% e 30%
X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)

pipe_svc = Pipeline([('scl', StandardScaler()),
            ('clf',RandomForestClassifier(random_state=0))])

param_range = [5, 10, 25, 50, 75, 100]

param_grid = [{'clf__n_estimators': param_range, 
                  'clf__criterion': ['entropy','gini'], 
                  'clf__max_features': ['sqrt','log2','auto']}]

gs = GridSearchCV(estimator=pipe_svc, 
                  param_grid=param_grid, 
                  scoring='accuracy', 
                  cv=10,
                  n_jobs=1)


gs = gs.fit(X_train, y_train)
print(gs.best_params_)
#######################################
clf = gs.best_estimator_

joblib.dump(clf, 'RF.pkl')

print(clf.score(X_train, y_train))
print(clf.score(X_test, y_test))
