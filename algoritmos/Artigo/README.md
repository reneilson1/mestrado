# Build instructions

To build the document with pdflatex:
```
make -f Makefile.pdflatex 
```
If you only have latex installed and requirements (i.e. ps2pdf, dvips), you can use:
```
make -f Makefile.latex
```
NOTE: for consistency, always make sure that the latex files are in UTF-8 charset.