import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.externals import joblib
from numpy import genfromtxt
from labels import labelsSVM
from scipy import signal
import pywt

def getArquivo(ruido, algoritmo,i):
    if algoritmo in ['w','f','n']:
        if(ruido != 0):
            X = genfromtxt(str(ruido)+'dB/' + algoritmo + '/conjunto_ruido_'+str(ruido)+'_' + str(i) + '_' + algoritmo + '.csv', delimiter=',')
        else:
            X = genfromtxt(str(ruido)+'dB/' + algoritmo + '/conjunto_ruido' + str(i) + '_' + algoritmo + '.csv', delimiter=',')
    elif (algoritmo == ''):
        if(ruido != 0):
            X = genfromtxt(str(ruido)+'dB/conjunto_ruido_'+str(ruido)+'_' + str(i) + '.csv', delimiter=',')
        else:
            X = genfromtxt(str(ruido)+'dB/conjunto_ruido' + str(i) + '.csv', delimiter=',')
    else:
        X = 0;
    return X;


#Variaveis uteis
ruidos = [0,20,30,50]
n=50
algoritmos = ['f','n','','w']

#Variaveis de Armazenamento de Metricas
acuracy = np.zeros(n)
acuracy_individual = np.zeros([8,n])
f1 = np.zeros(n)

#Criacao de labels (todos sao iguais para o SVM devido a quantidade 
# de elementos no vetor de entrada 4000)
y = labelsSVM(4000);

for ruido in ruidos:
    for algoritmo in algoritmos:
        #Classificador armazenado em arquivo (salvo apos gridSearch)
        classifier = joblib.load('SVM_' + algoritmo + '.pkl') 
        for i in range(n):
            #Ler o arquivo
            X = getArquivo(ruido, '', i+1)
            
            if(algoritmo == 'f'):
                #FFT 
                Y = np.fft.fft(X)
                L = X.shape[1]
                P2 = np.abs(Y/L)
                X = P2[:,1:np.round(L/2)+1]
            elif (algoritmo == 'w'):
                #Wavelet 
                cA, cD = pywt.dwt(X_aux, 'db4')
                X = cA
            elif (algoritmo == 'n'):
                #Filtro Nocth
                fs = 10000.0  # Sample frequency (Hz)
                f0 = 60.0  # Frequency to be removed from signal (Hz)
                Q = 30.0  # Quality factor
                w0 = f0/(fs/2)  # Normalized Frequency
                # Design notch filter
                b, a = signal.iirnotch(w0, Q)
                X = np.abs(signal.filtfilt(b,a,X))
            
            y_pred = classifier.predict(X)
            
            for j in range(8):
                res = (y_pred == y) & (y == j)
                acuracy_individual[j,i] = (np.argwhere(res==True).shape[0])/(
                        float(y.shape[0]/8))
                
            
            acuracy[i] = accuracy_score(y, y_pred)
            f1[i] = f1_score(y, y_pred, average='macro')
            
            #Imprime resultado
            print(classifier.score(X, y))
            print(f1[i])
        
        
        np.save('Accuracy_SVM_'+str(ruido)+'db_' + algoritmo, acuracy)
        np.save('Accuracy_SVM_'+str(ruido)+'dbEach_' + algoritmo, acuracy_individual)
        print(str(np.mean(acuracy)) + "+/-" + str(np.std(acuracy)))
        print(str(np.mean(f1)) + "+/-" + str(np.std(f1)))                                                                      


