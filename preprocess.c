#include <math.h>
const int SOMA = 0;
const int MEDIA = 1;
const int ENERGIA = 2;
const int NUM_MAX = 3;
const int VAL_MAX = 4;
const int NUM_MIN = 5;
const int VAL_MIN = 6;
const int RMS = 7;
const int MAX_ = 8;
const int MIN_ = 9;
const int SAMPLES = 512;

void separate (float* Xre, float* Xim, int n){
	float* Xre_aux =  malloc(sizeof(float) * n/2);
	float* Xim_aux = malloc(sizeof(float) * n/2);
	int i;
	
	for (i = 0; i < n/2; i++){
		Xre_aux[i] = Xre[i*2+1];
		Xim_aux[i] = Xim[i*2+1];
	}
	for (i = 0; i < n/2; i++){
		Xre[i] = Xre[i*2];
		Xim[i] = Xim[i*2];
	}
	for (i = 0; i < n/2; i++){
		Xre[i+n/2] = Xre_aux[i];
		Xim[i+n/2] = Xim_aux[i];
	}
}

void fft2(float* XRe, float* XIm, int N){
	if(N > 2){
		separate(XRe, XIm, N);
		fft(XRe, XIm, N/2);
		fft(XRe+N/2, XIm+N/2, N/2);

		for(k = 0; k < N/2; k++){
			wIm = sin(-2*PI*k/N);
			wRe = cos(-2*PI*k/N);

			XRe[k] += wRe * XRe[k + N/2];
			XRe[k+N/2] = XRe[k] - wRe * XRe[k + N/2];

			XIm[k] += wIm * XIm[k + N/2];
			XIm[k+N/2] = XIm[k] - wIm * XIm[k + N/2];
		}
	}
}

float* fft(float* dados){
	int tam = sizeof(dados)/sizeof(float);
	int i;

	float* Xre = malloc(sizeof(float) * SAMPLES);
	float* Xim = malloc(sizeof(float) * SAMPLES);

	float* X_fft = malloc(sizeof(float) * SAMPLES);

	for(i=0; i < tam; i++){
		Xre[i] = dados[i];
		Xim[i] = dados[i];
	}
	if(tam < SAMPLES){
		for(i=tam; i < SAMPLES; i++){
			Xre[i] = 0;
			Xim[i] = 0;
		}
	}
	
	fft2(Xre, Xim, SAMPLES);

	for (i = 0; i < SAMPLES; i++){
		X_fft[i] = Xre[i]*Xre[i] + Xim[i]*Xim[i];
	}

	return X_fft;
	 
}

float* func_vector(int choice, int tam, float *dados)
{
	int i;

	//Vetor de retorno: 
	//0- soma, 1- media, 2- energia
	//3- numero de maximos, 4- valores maximos
	//5- numero de minimos, 6- valores minimos
	//7- rms
	//8- maximo
	//9- minimo
	float vector[] = {0,0,0,0,0,0,0,0,0,0};

	for (i = 0; i < tam; i++)
		vector[0] = vector[0] + dados[i];  
		vector[2] = vector[2] + dados[i]*dados[i]  
		if (i > 0 && dados[i] > dados[i+1]) && (dados[i] > dados[i-1])){
			vector[3]++;
			vector[4] = vector[4] + (dados[i] - dados[i-1]);
		}
		if (i > 0 && dados[i] < dados[i+1]) && (dados[i] < dados[i-1])){
			vector[5]++;
			vector[6] = vector[6] + dados[i];
		}
		if (dados[i] > vector[8])
			vector[8] = dados[i];
		if (dados[i] < vector[9])
			vector[9] = dados[i];
		

	vector[1] = vector[0]/tam;
	vector[7] = sqrt(vector[2]/tam);
	
	return vector; 
}

float std_envelop(int tam, float *dados){
	float vector[]= {0,0,0,0};
	int i, j;
	float soma = 0, media =0, std = 0;

	for (j = 0; j < 4; j++){
		for (i = j*(tam/4); i < (j+1)*(tam/4); i++){
			soma = soma + dados[i];
		}
		vector[j] = soma / (tam/4);
		soma = 0;
		media = media + vector[j];
	} 
	media = media/4;

	for (i = 0; i < 4; i++){
		std = std + pow(vector[i] - media,2);
	}	

	std = sqrt(std/3);
	return std;
}

float* extremos(int mn, int mx, int tam, float* dados){
	int i;
	float maximo = 0, minimo = 0;
	float max_min[] = {0,0,0};
	for(i = mn; i < mx; i++){
		max_min[2] = max_min[2] + dados[i];
		if(i >= tam)
			i = i - tam;
		if (dados[i] > maximo)
			max_min[0] = dados[i];
		if (dados[i] < minimo)
			max_min[1] = dados[i];
	}
	max_min[2] = max_min[2]/(mx-mn);
	return max_min;
}

float* envelop(int tam, int w, float* dados){
	float* v_envelop = malloc(sizeof(float) * tam);
	int i;
	for (i = 0; i < tam; i++){
		v_envelop = extremos(j, j+w, tam, dados)[0];
	}
	return v_envelop;
}

float* construct_arg(float* dados){
	int tam_dados = sizeof(dados)/sizeof(float);

	float* X_fft = fft(dados);
	
	int tam_fft = sizeof(X_fft)/sizeof(float);
	tam_fft =/ 2;

	float* vector_dados = func_vector(tam_dados, dados);
	float* vector_fft = func_vector(tam_fft, X_fft);

	float std = std_envelop(tam_dados, dados);

	float* envelop_fft = envelop(tam_fft, 50, X_fft);
	float* envelop_dados = envelop(tam_dados, 100, dados);

	float* max_min_dados = extremos(0, tam_dados-50, tam_dados, envelop_dados);
	float* max_min_fft = extremos(0, tam_fft-20, tam_fft, envelop_fft);
	

	float X[] = {0,0,0,0,0,0,0,0,0,0,0,0};
	X[0] = vector_fft[SOMA];
	X[1] = vector_fft[VAL_MAX]*vector_fft[NUM_MAX];
	X[2] = vector_dados[RMS];
	X[3] = std;
	
	if(vector_dados[MAX_] < -vector_dados[MIN_])
		vector_dados[MAX_] = -vector_dados[MIN_];
	X[4] = vector_dados[MAX_] - vector_dados[MEDIA];
	X[5] = vector_dados[MEDIA] - vector_dados[MIN_];
	X[6] = vector_fft[NUM_MAX] - *vector_fft[NUM_MIN];
	X[7] = max_min_dados[0];
	X[8] = max_min_dados[1];
	X[9] = max_min_fft[0] - max_min_fft[1];
	X[10] = max_min_fft[0] - max_min_fft[2];
	X[11] = vector_dados[NUM_MAX]-3;

	return X;
}

int classifier(float soma_fft, float max_, float rms_dados, 
               float std_, float dist_max_mean_dados,
               float dist_min_mean_dados, float dist_cont,
               float min_env_dados, float max_env_dados,
               float dist_env_fft_mx, float dist_env_fft_mx_mean,
               float max_aux){
    if ( min_env_dados <= 0.9993405342102051 ) {
        if ( rms_dados <= 0.688856840133667 ) {
            if ( min_env_dados <= 0.10013996809720993 ) {
                return 1;
            } else {
                if ( dist_env_fft_mx <= 2.1340372562408447 ) {
                    if ( dist_min_mean_dados <= 1.073239803314209 ) {
                        if ( std_ <= 0.2932622730731964 ) {
                            return 2;
                        } else {
                            return 1;
                        }
                    } else {
                        if ( dist_env_fft_mx_mean <= 0.7787008881568909 ) {
                            return 2;
                        } else {
                            return 1;
                        }
                    }
                } else {
                    if ( max_ <= 18764.609375 ) {
                        return 1;
                    } else {
                        if ( soma_fft <= 59.26722717285156 ) {
                            return 2;
                        } else {
                            return 1;
                        }
                    }
                }
            }
        } else {
            if ( max_ <= 0.00017393683083355427 ) {
                if ( soma_fft <= 4.102768898010254 ) {
                    if ( dist_max_mean_dados <= 0.9998793005943298 ) {
                        if ( max_env_dados <= 0.9593193531036377 ) {
                            return 6;
                        } else {
                            return 0;
                        }
                    } else {
                        return 5;
                    }
                } else {
                    if ( max_env_dados <= 1.0228557586669922 ) {
                        if ( max_env_dados <= 0.971593976020813 ) {
                            return 6;
                        } else {
                            return 0;
                        }
                    } else {
                        return 6;
                    }
                }
            } else {
                if ( max_env_dados <= 1.0003225803375244 ) {
                    if ( soma_fft <= 7.246959686279297 ) {
                        return 2;
                    } else {
                        return 8;
                    }
                } else {
                    if ( dist_env_fft_mx_mean <= 1.623831033706665 ) {
                        return 9;
                    } else {
                        return 6;
                    }
                }
            }
        }
    } else {
        if ( max_aux <= 2.5 ) {
            if ( dist_min_mean_dados <= 1.019820213317871 ) {
                if ( max_ <= 2.6601304625728517e-07 ) {
                    if ( dist_env_fft_mx_mean <= 0.0754564106464386 ) {
                        if ( dist_max_mean_dados <= 1.0000007152557373 ) {
                            return 8;
                        } else {
                            return 5;
                        }
                    } else {
                        if ( dist_min_mean_dados <= 1.0012495517730713 ) {
                            return 0;
                        } else {
                            return 8;
                        }
                    }
                } else {
                    if ( dist_min_mean_dados <= 1.000420331954956 ) {
                        if ( dist_min_mean_dados <= 1.0000941753387451 ) {
                            return 4;
                        } else {
                            return 7;
                        }
                    } else {
                        if ( soma_fft <= 6.391940116882324 ) {
                            return 3;
                        } else {
                            return 8;
                        }
                    }
                }
            } else {
                if ( rms_dados <= 0.720308780670166 ) {
                    if ( rms_dados <= 0.7111003398895264 ) {
                        return 4;
                    } else {
                        if ( max_ <= 5818.8583984375 ) {
                            return 6;
                        } else {
                            return 9;
                        }
                    }
                } else {
                    return 3;
                }
            }
        } else {
            if ( min_env_dados <= 1.0003288984298706 ) {
                if ( dist_env_fft_mx_mean <= 0.05461488664150238 ) {
                    if ( dist_min_mean_dados <= 1.0001726150512695 ) {
                        return 4;
                    } else {
                        if ( dist_min_mean_dados <= 1.0079435110092163 ) {
                            return 7;
                        } else {
                            return 4;
                        }
                    }
                } else {
                    return 4;
                }
            } else {
                return 6;
            }
        }
    }
}

int main(){
	float* f = construc_arg(dados);
	int class = classifier(f[0], f[1], f[2], f[3], f[4], f[5],
				f[6], f[7], f[8], f[9], f[10], f[11]);
	
}
