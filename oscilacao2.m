function [o, o_f, o_w, o_n] = oscilacao2(N, ruido)
    t0 = 0:0.0001:3/60;
    t1 = 0.001:0.00005:0.002;
    t2 = 0:0.0001:57/60;
    
    %Senoide pura
    h0 = sin(2*pi*t0*60);
    
    %Inicializando vari�veis de retorno
    o = cell(1);
    o_w = cell(1);
    o_f = cell(1);
    o_n = cell(1);
    
    %Par�metros do filtro notch
    wo = 60/(10*size(t0,2)); bw=wo/35;
    [b, a] = iirnotch(wo, bw);

    for j = 1:N
        %Amplitude da oscila��o (entre 0.01 e 0.3)
        r1 = 0.001*round(10+rand(1)*290);
        
        %Par�metros da oscila��o
        beta=round(4+rand(1)*2);
        gama=round(800+rand(1)*100);
        freq_o=round(10000+rand(1)*5000);

        h1 = beta*exp(-gama*t1);
        h2 = sin(2*pi*t1*freq_o);
        h3 = h1.*h2;
        
        %Onde come�a a oscila��o
        rn = round(rand(1)*(size(h0,2)-size(h3,2)));
        h3 = [zeros(1,rn) h3 zeros(1,size(h0,2)-rn-size(h3,2))];
        
        %Vari�vel com sen�ide e oscila��o
        aux = h0+r1*h3;
        if (ruido ~= 0)
            aux = add_awgn_noise(aux, ruido);
        end
        o{j,1} = aux;
        
        %Gerando sinal com notch
        hf = filter(b, a, [sin(2*pi*t2*60) aux]);
        o_n{j,1} = hf(:,size(t2,2)+1:size(hf,2));
        
        %Gerando sinal com FFT
        Y = fft(aux);
        L = size(t0,2);
        P2 = abs(Y/L);
        P1 = P2(1:round(L/2)+1);
        P1(2:end-1) = 2*P1(2:end-1);
        
        o_f{j,1} = P1;
        
        %Gerando sinal com Wavelet
        [C, L] =  wavedec(aux, 4, 'db4');
        d1 = appcoef(C, L, 'db4', 1);
        
        o_w{j,1} = d1;
    end
    %Transformando em matriz
    o = cell2mat(o);
    o_w = cell2mat(o_w);
    o_f = cell2mat(o_f);
    o_n = cell2mat(o_n);
end