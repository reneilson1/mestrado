from sklearn.grid_search import GridSearchCV
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.cross_validation import train_test_split
from sklearn.externals import joblib
from plot_confusion import plot_confusion_matrix
from sklearn.metrics import confusion_matrix

from scipy import signal
import pywt
import numpy as np
import matplotlib.pyplot as plt
import scipy

from numpy import genfromtxt
from labels import labelsRNA

algoritmos = ['f','w','','n']

#def getArquivo(ruido, algoritmo,i):
#    if algoritmo in ['w','f','n']:
#        if(ruido != 0):
#            X = genfromtxt(str(ruido)+'dB/' + algoritmo + '/conjunto_ruido_'+str(ruido)+'_' + str(i) + '_' + algoritmo + '.csv', delimiter=',')
#        else:
#            X = genfromtxt(str(ruido)+'dB/' + algoritmo + '/conjunto_ruido' + str(i) + '_' + algoritmo + '.csv', delimiter=',')
#    elif (algoritmo == ''):
#        if(ruido != 0):
#            X = genfromtxt(str(ruido)+'dB/conjunto_ruido_'+str(ruido)+'_' + str(i) + '.csv', delimiter=',')
#        else:
#            X = genfromtxt(str(ruido)+'dB/conjunto_ruido' + str(i) + '.csv', delimiter=',')
#    else:
#        X = 0;
#    return X;
#
#X_0dB  = getArquivo(0 , algoritmo, 0)
#X_20dB = getArquivo(20, algoritmo, 0)
#X_30dB = getArquivo(30, algoritmo, 0)
#X_50dB = getArquivo(50, algoritmo, 0)
#
#X = np.concatenate((X_0dB[0:40000], X_20dB[0:4000],X_30dB[0:4000],X_50dB[0:4000]))
X_aux = np.load('dados.npy')

for algoritmo in algoritmos:
    if(algoritmo == 'f'):
        #FFT 
        Y = np.fft.fft(X_aux)
        L = X_aux.shape[1]
        P2 = np.abs(Y/L)
        X = P2[:,1:np.round(L/2)+1]
        del Y, L, P2
    elif (algoritmo == 'w'):
        #Wavelet 
        cA, cD = pywt.dwt(X_aux, 'db4')
        X = cA
        del cA
    elif (algoritmo == 'n'):
        #Filtro Nocth
        fs = 10000.0  # Sample frequency (Hz)
        f0 = 60.0  # Frequency to be removed from signal (Hz)
        Q = 30.0  # Quality factor
        w0 = f0/(fs/2)  # Normalized Frequency
        # Design notch filter
        b, a = signal.iirnotch(w0, Q)
        X = np.abs(signal.filtfilt(b,a,X_aux))
        del fs, f0, Q, w0,b,a
    
#    if algoritmo == 'w':
#        aux = np.zeros([32,X.shape[0]])
#        aux[0] = np.mean(X,axis=1)
#        aux[1] = np.std(X, axis=1)
#        aux[2] = np.max(X, axis=1)
#        aux[3] = np.min(X, axis=1)
#        aux[4] = np.sum(X, axis=1)
#        aux[5] = scipy.stats.kurtosis(X,axis=1)
#        aux[6] = 0#scipy.stats.gmean(X,axis=1)
#        aux[7] = 0#scipy.stats.hmean(np.abs(X),axis=1)
#        aux[8] = np.sqrt(np.mean(X**2))
#        aux[9] = scipy.stats.moment(X,axis=1)
#        aux[10] = scipy.stats.skew(X,axis=1)
#        aux[11] = scipy.stats.tmean(X,axis=1)
#        aux[12] = scipy.stats.tvar(X,axis=1)
#        aux[13] = scipy.stats.tstd(X,axis=1)
#        aux[14] = np.mean(cD,axis=1)
#        aux[15] = np.std(cD, axis=1)
#        aux[16] = np.max(cD, axis=1)
#        aux[17] = np.min(cD, axis=1)
#        aux[18] = np.sum(cD, axis=1)
#        aux[19] = scipy.stats.kurtosis(cD,axis=1)
#        aux[20] = np.sqrt(np.mean(cD**2))
#        aux[21] = scipy.stats.moment(cD,axis=1)
#        aux[22] = scipy.stats.skew(cD,axis=1)
#        aux[23] = scipy.stats.tmean(cD,axis=1)
#        aux[24] = scipy.stats.tvar(cD,axis=1)
#        aux[25] = scipy.stats.tstd(cD,axis=1)
#        aux[26] = 0#scipy.stats.entropy(X.T)
#        aux[27] = 0#scipy.stats.entropy(cD.T)
#        aux[28] = np.mean(scipy.signal.welch(X,axis=1)[1], axis=1)
#        aux[29] = np.mean(scipy.signal.welch(cD,axis=1)[1], axis=1)
#        aux[30] = np.sum(np.abs(X)**2, axis=1)
#        aux[31] = np.sum(np.abs(cD)**2, axis=1)
#    else:
#        aux = np.zeros([14,X.shape[0]])
#        aux[0] = np.mean(X,axis=1)
#        aux[1] = np.std(X, axis=1)
#        aux[2] = np.max(X, axis=1)
#        aux[3] = np.min(X, axis=1)
#        aux[4] = np.sum(X, axis=1)
#        aux[5] = scipy.stats.kurtosis(X,axis=1)
#        aux[6] = np.sqrt(np.mean(X**2))
#        aux[7] = scipy.stats.moment(X,axis=1)
#        aux[8] = scipy.stats.skew(X,axis=1)
#        aux[9] = scipy.stats.tmean(X,axis=1)
#        aux[10] = scipy.stats.tvar(X,axis=1)
#        aux[11] = scipy.stats.tstd(X,axis=1)
#        aux[12] = np.mean(scipy.signal.welch(X,axis=1)[1], axis=1)
#        aux[13] = np.sum(np.abs(X)**2, axis=1)
    
    #X = aux.T
    
    #X = X[0:8000,:]
    y = labelsRNA(X.shape[0]).T
    
    # define os dados de treino e os dados de teste 70% e 30%
    X_train, X_test, y_train, y_test = train_test_split(
             X, y, test_size=0.3, random_state=1)
    
    
    pipe_svc = Pipeline([('scl', StandardScaler()),
                ('clf',MLPClassifier(random_state=1, max_iter=500, 
                                     early_stopping=True))])
    
    #param_solver = ['lbfgs', 'sgd', 'adam']
    #param_ativacao = ['identity', 'logistic', 'tanh', 'relu']
    #param_alpha = [1e+0, 1e-1, 1e-2, 1e-3]
    #param_layer = [(2,2),(5,2),(10,2),(15,2)]
    
    param_solver = ['lbfgs']
    param_ativacao = ['tanh']
    param_alpha = [1e-2]
    param_layer = [(10), (50), (100)]
    
    param_grid = [{'clf__solver': param_solver,
                   'clf__activation': param_ativacao,
                   'clf__alpha': param_alpha,
                   'clf__hidden_layer_sizes': param_layer}]
    
    gs = GridSearchCV(estimator=pipe_svc, 
                      param_grid=param_grid, 
                      scoring='accuracy', 
                      cv=2,
                      n_jobs=2,
                      verbose=1)
    
    gs = gs.fit(X_train, y_train)
    
    print(gs.best_params_)
    
    clf = gs.best_estimator_
    
    print(clf.score(X_train, y_train))
    print(clf.score(X_test, y_test))
    joblib.dump(clf, 'MLP_' + algoritmo +'.pkl')