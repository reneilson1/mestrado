import numpy as np
from statsmodels.stats.weightstats import ztest

ac10 = np.load('ac10.npy')
ac20 = np.load('ac20.npy')

print (ztest(ac10, value=0.8, alternative='larger'))
print (ztest(ac20, value=0.9, alternative='larger'))