function [h, h_f, h_w, h_n] = harmonica2(N, ruido)
    t0 = 0:0.0001:3/60;
    t2 = 0:0.0001:57/60;
    
    %Inicializando vari�veis de retorno
    h = cell(1);
    h_w = cell(1);
    h_f = cell(1);
    h_n = cell(1);
    
    %Par�metros do filtro notch
    wo = 60/(10*size(t0,2)); bw=wo/35;
    [b, a] = iirnotch(wo, bw);

    for j = 1:N
        %Senoide pura
        h0 = sin(2*pi*t0*60);
        %Numero de harmonicas
        harm = round(rand(1)*5);

        for i = 1:harm
            %Amplitude das harmonicas
            Am = 0.001*(50+(round(rand(1)*150)));
            %Frequ�ncia das harm�nicas
            hm = 2+round(rand(1)*38);
            %Sinal com harm�nica
            h0 = h0 + Am*sin(2*pi*t0*60*hm);
        end
        
        %Vari�vel com sen�ide pura
        aux = h0;
        if (ruido ~= 0)
            aux = add_awgn_noise(aux, ruido);
        end
        h{j,1} = aux;
        
        %Gerando sinal com notch
        hf = filter(b, a, [sin(2*pi*t2*60) aux]);
        h_n{j,1} = hf(:,size(t2,2)+1:size(hf,2));
        
        %Gerando sinal com FFT
        Y = fft(aux);
        L = size(t0,2);
        P2 = abs(Y/L);
        P1 = P2(1:round(L/2)+1);
        P1(2:end-1) = 2*P1(2:end-1);
        
        h_f{j,1} = P1;
        
        %Gerando sinal com Wavelet
        [C, L] =  wavedec(aux, 4, 'db4');
        d1 = appcoef(C, L, 'db4', 1);
        
        h_w{j,1} = d1;
    end
    %Transformando em matriz
    h = cell2mat(h);
    h_w = cell2mat(h_w);
    h_f = cell2mat(h_f);
    h_n = cell2mat(h_n);
end