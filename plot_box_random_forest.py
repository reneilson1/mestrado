import matplotlib.pyplot as plt
import numpy as np

cnf_matrix = np.load('conf_0.npy')
cnf_matrix1 = np.load('conf_10.npy')
cnf_matrix2 = np.load('conf_20.npy')
cnf_matrix3 = np.load('conf_30.npy')
cnf_matrix4 = np.load('conf_50.npy')

for i in range(50):
    for j in range(10):
        for k in range(10):
            cnf_matrix[i,j,k] = cnf_matrix[i,j,k]/200
            cnf_matrix1[i,j,k] = cnf_matrix1[i,j,k]/200
            cnf_matrix2[i,j,k] = cnf_matrix2[i,j,k]/200
            cnf_matrix3[i,j,k] = cnf_matrix3[i,j,k]/200
            cnf_matrix4[i,j,k] = cnf_matrix4[i,j,k]/200

c0 = np.zeros([50,10])
c1 = np.zeros([50,10])
c2 = np.zeros([50,10])
c3 = np.zeros([50,10])
c4 = np.zeros([50,10])

for i in range(50):
    for j in range(10):
        c0[i,j] = cnf_matrix[i, j, j]
        c1[i,j] = cnf_matrix1[i, j, j]
        c2[i,j] = cnf_matrix2[i, j, j]
        c3[i,j] = cnf_matrix3[i, j, j]
        c4[i,j] = cnf_matrix4[i, j, j]
        

plt.figure()
plt.boxplot(c0)
plt.xticks([1,2,3,4,5,6,7,8,9,10], ['Seno', 'Interrupcao', 'Sag', 'Swell',
           'Oscilacao', 'Flicker', 'Harmonicas', 'Notching', 'Impulsivo',
           'Nivel DC'], rotation='vertical')
plt.savefig('0dB.png')

plt.figure()
plt.boxplot(c1)
plt.xticks([1,2,3,4,5,6,7,8,9,10], ['Seno', 'Interrupcao', 'Sag', 'Swell',
           'Oscilacao', 'Flicker', 'Harmonicas', 'Notching', 'Impulsivo',
           'Nivel DC'], rotation='vertical')
plt.savefig('10dB.png')

plt.figure()
plt.boxplot(c2)
plt.xticks([1,2,3,4,5,6,7,8,9,10], ['Seno', 'Interrupcao', 'Sag', 'Swell',
           'Oscilacao', 'Flicker', 'Harmonicas', 'Notching', 'Impulsivo',
           'Nivel DC'], rotation='vertical')
plt.savefig('20dB.png')

plt.figure()
plt.boxplot(c3)
plt.xticks([1,2,3,4,5,6,7,8,9,10], ['Seno', 'Interrupcao', 'Sag', 'Swell',
           'Oscilacao', 'Flicker', 'Harmonicas', 'Notching', 'Impulsivo',
           'Nivel DC'], rotation='vertical')
plt.savefig('30dB.png')

plt.figure()
plt.boxplot(c4)
plt.xticks([1,2,3,4,5,6,7,8,9,10], ['Seno', 'Interrupcao', 'Sag', 'Swell',
           'Oscilacao', 'Flicker', 'Harmonicas', 'Notching', 'Impulsivo',
           'Nivel DC'], rotation='vertical')
plt.savefig('50dB.png')