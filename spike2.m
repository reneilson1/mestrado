function [spk, spk_f, spk_w, spk_n] = spike2(N, ruido)
    t0 = 0:0.0001:3/60;
    t2 = 0:0.0001:57/60;
    
    %Senoide pura
    h0 = sin(2*pi*t0*60);
    
    %Inicializando vari�veis de retorno
    spk = cell(1);
    spk_w = cell(1);
    spk_f = cell(1);
    spk_n = cell(1);
    
    %Par�metros do filtro notch
    wo = 60/(10*size(t0,2)); bw=wo/35;
    [b, a] = iirnotch(wo, bw);

    for j = 1:N
        %Onde come�a e termina o afundamento de tens�o
         r1 = 1+round(rand(1)*100);
        %Amplitude do pico
        alpha = 0.1 + 0.001*(round(rand(1)*400));
        
        h1 = zeros(size(h0));
        h1(:,r1) = 1;
        h1 = h0+alpha*h1;
        
        %Vari�vel com sen�ide pura
        aux = h1;
        if (ruido ~= 0)
            aux = add_awgn_noise(aux, ruido);
        end
        spk{j,1} = aux;
        
        %Gerando sinal com notch
        hf = filter(b, a, [sin(2*pi*t2*60) aux]);
        spk_n{j,1} = hf(:,size(t2,2)+1:size(hf,2));
        
        %Gerando sinal com FFT
        Y = fft(aux);
        L = size(t0,2);
        P2 = abs(Y/L);
        P1 = P2(1:round(L/2)+1);
        P1(2:end-1) = 2*P1(2:end-1);
        
        spk_f{j,1} = P1;
        
        %Gerando sinal com Wavelet
        [C, L] =  wavedec(aux, 4, 'db4');
        d1 = appcoef(C, L, 'db4', 1);
        
        spk_w{j,1} = d1;
    end
    %Transformando em matriz
    spk = cell2mat(spk);
    spk_w = cell2mat(spk_w);
    spk_f = cell2mat(spk_f);
    spk_n = cell2mat(spk_n);
end   