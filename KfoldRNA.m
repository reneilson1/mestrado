function [per,perT] = KfoldRNA(baseDados, labels)
    per = cell(1); perT = cell(1);
    clear 'net';
 
    % Create a Pattern Recognition Network
    hiddenLayerSize = 10;
    net = patternnet(hiddenLayerSize);
    %         net.performFcn = 'mse';
    %         net.trainFcn = 'trainscg';
%     net.divideFcn='dividetrain';
%     net.trainParam.epochs = 1000;
    %         net.trainParam.gama = 5.0e-4;
%     net.trainParam.max_fail = 6;

    [trainInd,valInd,testInd] = dividerand(size(labels,2),0.8,0.2,0);

       
    net = init(net);
    
    Teste = baseDados(:,valInd);
    LabelsT = labels(:,valInd);

    Treino = baseDados(:,trainInd);
    LabelTreino = labels(:,trainInd);

    [net,tr] = train(net,Treino,LabelTreino);
    outputs = net(Teste);
    perT{1} = confusion(LabelsT,outputs);

    for j=1:8
        k = j:8:size(LabelsT, 2);
        per{1,j} = confusion(LabelsT(:,k), outputs(:,k));
        if(per{1,j} > 0.2)
%                 figure, plotconfusion(LabelsT, outputs);
        end
    end
        
end
