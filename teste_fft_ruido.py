import numpy as np
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import train_test_split
from sklearn.metrics import confusion_matrix
from Bucket import Bucket
from scipy.signal import hilbert
from scipy.ndimage import filters
from plot_confusion import plot_confusion_matrix
import scipy.fftpack

b = Bucket(500,20,freq=60)
dados = b.createBucket()
y = b.createLabelsSVM()

y1 = np.zeros(y.shape)
for i in range(y.shape[0]):
    if y[i] in [6]:
        y1[i] = 1

df=filters.gaussian_filter1d(dados,10)

Y = scipy.fftpack.fft(dados)
L = dados.shape[1]
P2 = np.abs(2*Y/L)
X_ = P2[:,1:np.round(L/2)+1]
del Y, L, P2

X = np.zeros([X_.shape[0], X_.shape[1]])

for i in range(X_.shape[0]):
    for j in range(X_.shape[1]):
        for k in range(X_.shape[1]):
            X[i,j] = X_[i, k]*X_[i, (k+j)%(X_.shape[1])] + X[i,j]
#            X3[i,j] = X_[i, k]*(X_**3)[i, (k+j)%(X_.shape[1])] + X[i,j]
        X[i,j] = X[i,j]/X_.shape[1]
#        X3[i,j] = X3[i,j]/X_.shape[1] - (1/(X_.shape[1]**2)*X[i,j])*np.sum(X[i])
        
Xi = np.array([np.max(X, axis=1),
              np.max(X[:,10:], axis=1),
              np.sum(X,axis=1),
              ]).T
    
    
X_train, X_test, y_train, y_test = train_test_split(
         Xi, y, test_size=0.3, random_state=0)

clf = DecisionTreeClassifier(random_state=1, criterion='entropy',
                             max_depth=6)
clf.fit(X_train, y_train)

print(clf.score(X_test, y_test))
print(clf.score(X_train, y_train))

######################EXTRAS####################################
y_pred = clf.predict(X_test)

cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['0','1','2','3','4','5',
                                           '6','7','8','9'],
                      normalize=True,
                      title='Confusion matrix, without normalization')

plt.show()
clf.feature_importances_