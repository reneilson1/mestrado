for k=1:50

    savefile = strcat('conjunto_ruido_20_', mat2str(k), '.mat')
%     load(savefile);
    N = 500;
    ruido = 20;    
% % 
    [nm, nm_f, nm_w, nm_n] = normal2(N, ruido);
    [n, n_f, n_w, n_n] = notching2(N, ruido);
    [o, o_f, o_w, o_n] = oscilacao2(N, ruido);
    [sg, sg_f, sg_w, sg_n] = sag2(N, ruido);
    [sw, sw_f, sw_w, sw_n] = swell2(N, ruido);
    [i, i_f, i_w, i_n] = interrupcao2(N, ruido);
    [spk, spk_f, spk_w, spk_n] = spike2(N, ruido);
    [h, h_f, h_w, h_n] = harmonica2(N, ruido);

    [trainDataR,LabelR,trainDataSVM,LabelsSVM] = BaseDados(nm,h,i,o,sg,sw,spk,n,N);
    [trainDataR_f,LabelR_f,trainDataSVM_f,LabelsSVM_f] = BaseDados(nm_f,h_f,i_f,o_f,sg_f,sw_f,spk_f,n_f,N);
    [trainDataR_n,LabelR_n,trainDataSVM_n,LabelsSVM_n] = BaseDados(nm_n,h_n,i_n,o_n,sg_n,sw_n,spk_n,n_n,N);
    [trainDataR_w,LabelR_w,trainDataSVM_w,LabelsSVM_w] = BaseDados(nm_w,h_w,i_w,o_w,sg_w,sw_w,spk_w,n_w,N);

    LabelsSVM = zeros(8,8*N);
    LabelsSVM_f = zeros(8,8*N);
    LabelsSVM_w = zeros(8,8*N);
    LabelsSVM_n = zeros(8,8*N);
    j=1;
    for i = 1:N
        for j=1:8
            LabelsSVM(j,(i-1)*8+j) = 1;
            LabelsSVM_f(j,(i-1)*8+j) = 1;
            LabelsSVM_n(j,(i-1)*8+j) = 1;
            LabelsSVM_w(j,(i-1)*8+j) = 1;
        end
    end

%     [RNAper,RNAperT] = KfoldRNA(trainDataSVM', LabelsSVM);
%     [RNAper_f,RNAperT_f] = KfoldRNA(trainDataSVM_f', LabelsSVM_f);
%     [RNAper_n,RNAperT_n] = KfoldRNA(trainDataSVM_n', LabelsSVM_n);
%     [RNAper_w,RNAperT_w] = KfoldRNA(trainDataSVM_w', LabelsSVM_w);
% 
% %     [SVMper,SVMperT] = Kfold(trainDataSVM, LabelsSVM);
% %     [SVMper_f,SVMperT_f] = Kfold(trainDataSVM_f, LabelsSVM_f);
% %     [SVMper_w,SVMperT_w] = Kfold(trainDataSVM_w, LabelsSVM_w);
% %     [SVMper_n,SVMperT_n] = Kfold(trainDataSVM_n, LabelsSVM_n);
% 
% %     perSVM{j} = SVMper;
% %     perSVM_f{j} = SVMper_f;
% %     perSVM_w{j} = SVMper_w;
% %     perSVM_n{j} = SVMper_n;
    save(savefile, 'trainDataSVM','LabelsSVM', ...
        'trainDataSVM_f','LabelsSVM_f', ...
        'trainDataSVM_w','LabelsSVM_w', ...
        'trainDataSVM_n','LabelsSVM_n');
%     savefile2 = strcat('percentualRNA', mat2str(k), '.mat');
%     save(savefile2, 'RNAper', 'RNAperT','RNAper_f', 'RNAperT_f', ...
%         'RNAper_w', 'RNAperT_w', 'RNAper_n', 'RNAperT_n');
    
    clear;
end