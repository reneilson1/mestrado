from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import train_test_split
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from pywt import wavedec

import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

X = np.load('dados.npy');
y = np.load('target.npy');

#Coeficientes DWT
coeffs = wavedec(X,'db2', level=6)
cA6, cD6, cD5, cD4, cD3, cD2, cD1 = coeffs

e_cD6 = (cD6**2).sum(axis=1)
e_cD5 = (cD5**2).sum(axis=1)
e_cD4 = (cD4**2).sum(axis=1)
e_cD3 = (cD3**2).sum(axis=1)
e_cD2 = (cD2**2).sum(axis=1)
e_cD1 = (cD1**2).sum(axis=1)

m_cD6 = np.mean(cD6, axis=1)
m_cD5 = np.mean(cD5, axis=1)
m_cD4 = np.mean(cD4, axis=1)
m_cD3 = np.mean(cD3, axis=1)
m_cD2 = np.mean(cD2, axis=1)
m_cD1 = np.mean(cD1, axis=1)

s6 = stats.skew(cD6,axis=1)
s5 = stats.skew(cD5,axis=1)
s4 = stats.skew(cD4,axis=1)
s3 = stats.skew(cD3,axis=1)
s2 = stats.skew(cD2,axis=1)
s1 = stats.skew(cD1,axis=1)

X = np.array([e_cD1, e_cD2, e_cD3, 
                  e_cD4, e_cD5, e_cD6, 
                  m_cD1, m_cD2, m_cD3, 
                  m_cD4, m_cD5, m_cD6,
                  s6, s5, s4, s3, s2, s1])

X = X.T


# define os dados de treino e os dados de teste 70% e 30%
X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)
"""
sc = StandardScaler()
sc.fit(X_train)
X_train = sc.transform(X_train)
X_test = sc.transform(X_test)
"""
forest = RandomForestClassifier(n_estimators=100,
                                criterion='entropy',
                                max_features='sqrt',
                                random_state=0,
                                n_jobs=1)

forest.fit(X_train, y_train)
print forest.score(X_train, y_train)
print forest.score(X_test, y_test)
importances = forest.feature_importances_

indices = np.argsort(importances)[::-1]

for f in range(X_train.shape[1]):
    print("%2d) %-*s %f" % (f + 1, 30, 
                            indices[f], 
                            importances[indices[f]]))

plt.title('Feature Importances')
plt.bar(range(X_train.shape[1]), 
        importances[indices],
        color='lightblue', 
        align='center')

plt.xticks(range(X_train.shape[1]), 
           indices, rotation=90)
plt.xlim([-1, X_train.shape[1]])
plt.tight_layout()
#plt.savefig('./random_forest.png', dpi=300)
plt.show()

np.save('indices', indices)
