function [per,perT] = Kfold(baseDados, labels)
    per = cell(1); perT = cell(1);
    
    Teste = baseDados(1:8000,:);
    LabelsT = labels(1:8000,1);

    Treino = baseDados(8001:end,:);
    LabelTreino = labels(8001:end,1);
        
    classifier = fitcecoc(Treino,LabelTreino);
    classificacao = predict(classifier,Teste);
    
    for j = 1:8
        aux = find(classificacao == LabelsT & classificacao == j);
        per{1,j} = size(aux,1)/size(find(j==LabelsT),1);
    end          
    auxT = find(classificacao == LabelsT);
    perT{1} = size(auxT,1)/size(Teste,1);
    
end
