from sklearn.grid_search import GridSearchCV
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from sklearn.cross_validation import train_test_split
from sklearn.externals import joblib
from plot_confusion import plot_confusion_matrix
from sklearn.metrics import confusion_matrix

from scipy import signal
import pywt
import numpy as np
import matplotlib.pyplot as plt
from Bucket import Bucket

#ruido_array = [0,20,30,50]
algoritmo_array = ['w','f','n','']

#def getArquivo(ruido, algoritmo,i):
#    if algoritmo in ['w','f','n']:
#        if(ruido != 0):
#            X = genfromtxt(str(ruido)+'dB/' + algoritmo + '/conjunto_ruido_'+str(ruido)+'_' + str(i) + '_' + algoritmo + '.csv', delimiter=',')
#        else:
#            X = genfromtxt(str(ruido)+'dB/' + algoritmo + '/conjunto_ruido' + str(i) + '_' + algoritmo + '.csv', delimiter=',')
#    elif (algoritmo == ''):
#        if(ruido != 0):
#            X = genfromtxt(str(ruido)+'dB/conjunto_ruido_'+str(ruido)+'_' + str(i) + '.csv', delimiter=',')
#        else:
#            X = genfromtxt(str(ruido)+'dB/conjunto_ruido' + str(i) + '.csv', delimiter=',')
#    else:
#        X = 0;
#    return X;

#X_0dB  = getArquivo(0, '', 0)
#X_20dB = getArquivo(20, '', 0)
#X_30dB = getArquivo(30, '', 0)
#X_50dB = getArquivo(50, '', 0)
#
#X_aux = np.concatenate((X_0dB[0:40000], X_20dB[0:4000],X_30dB[0:4000],X_50dB[0:4000]))
#
#del X_0dB, X_20dB, X_30dB, X_50dB
    
#X_aux = np.load("dados.npy")
b = Bucket(500, 0, 60, False)
X_aux = b.createBucket()
N = X_aux.shape[0]

y = np.zeros(N)
for i in range(N):
    y[i] = i%16

for algoritmo in algoritmo_array:
    
    if(algoritmo == 'f'):
        #FFT 
        Y = np.fft.fft(X_aux)
        L = X_aux.shape[1]
        P2 = np.abs(Y/L)
        X = P2[:,1:np.round(L/2)+1]
        del Y, L, P2
    elif (algoritmo == 'w'):
        #Wavelet 
        cA, cD = pywt.dwt(X_aux, 'db4')
        X = np.concatenate((cA,cD), axis=1)
        del cA, cD
    elif (algoritmo == 'n'):
        #Filtro Nocth
        fs = 10000.0  # Sample frequency (Hz)
        f0 = 60.0  # Frequency to be removed from signal (Hz)
        Q = 30.0  # Quality factor
        w0 = f0/(fs/2)  # Normalized Frequency
        # Design notch filter
        b, a = signal.iirnotch(w0, Q)
        X = np.abs(signal.filtfilt(b,a,X_aux))
        del fs, f0, Q, w0, b, a
    else:
        X = X_aux
        
#    if algoritmo == 'w':
#        aux = np.zeros([32,X.shape[0]])
#        aux[0] = np.mean(X,axis=1)
#        aux[1] = np.std(X, axis=1)
#        aux[2] = np.max(X, axis=1)
#        aux[3] = np.min(X, axis=1)
#        aux[4] = np.sum(X, axis=1)
#        aux[5] = scipy.stats.kurtosis(X,axis=1)
#        aux[6] = 0#scipy.stats.gmean(X,axis=1)
#        aux[7] = 0#scipy.stats.hmean(np.abs(X),axis=1)
#        aux[8] = np.sqrt(np.mean(X**2))
#        aux[9] = scipy.stats.moment(X,axis=1)
#        aux[10] = scipy.stats.skew(X,axis=1)
#        aux[11] = scipy.stats.tmean(X,axis=1)
#        aux[12] = scipy.stats.tvar(X,axis=1)
#        aux[13] = scipy.stats.tstd(X,axis=1)
#        aux[14] = np.mean(cD,axis=1)
#        aux[15] = np.std(cD, axis=1)
#        aux[16] = np.max(cD, axis=1)
#        aux[17] = np.min(cD, axis=1)
#        aux[18] = np.sum(cD, axis=1)
#        aux[19] = scipy.stats.kurtosis(cD,axis=1)
#        aux[20] = np.sqrt(np.mean(cD**2))
#        aux[21] = scipy.stats.moment(cD,axis=1)
#        aux[22] = scipy.stats.skew(cD,axis=1)
#        aux[23] = scipy.stats.tmean(cD,axis=1)
#        aux[24] = scipy.stats.tvar(cD,axis=1)
#        aux[25] = scipy.stats.tstd(cD,axis=1)
#        aux[26] = 0#scipy.stats.entropy(X.T)
#        aux[27] = 0#scipy.stats.entropy(cD.T)
#        aux[28] = np.mean(scipy.signal.welch(X,axis=1)[1], axis=1)
#        aux[29] = np.mean(scipy.signal.welch(cD,axis=1)[1], axis=1)
#        aux[30] = np.sum(np.abs(X)**2, axis=1)
#        aux[31] = np.sum(np.abs(cD)**2, axis=1)
#    else:
#        aux = np.zeros([14,X.shape[0]])
#        aux[0] = np.mean(X,axis=1)
#        aux[1] = np.std(X, axis=1)
#        aux[2] = np.max(X, axis=1)
#        aux[3] = np.min(X, axis=1)
#        aux[4] = np.sum(X, axis=1)
#        aux[5] = scipy.stats.kurtosis(X,axis=1)
#        aux[6] = np.sqrt(np.mean(X**2))
#        aux[7] = scipy.stats.moment(X,axis=1)
#        aux[8] = scipy.stats.skew(X,axis=1)
#        aux[9] = scipy.stats.tmean(X,axis=1)
#        aux[10] = scipy.stats.tvar(X,axis=1)
#        aux[11] = scipy.stats.tstd(X,axis=1)
#        aux[12] = np.mean(scipy.signal.welch(X,axis=1)[1], axis=1)
#        aux[13] = np.sum(np.abs(X)**2, axis=1)
#    
#    X = aux.T
    
    # define os dados de treino e os dados de teste 70% e 30%
    X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.3, random_state=1)
                
                
    pipe_svc = Pipeline([('scl', StandardScaler()),
                         ('clf',SVC(random_state=1))])
                
    param_range = [1e+6, 1e+4]
                
    param_grid = [{'clf__C': param_range,
                   'clf__decision_function_shape': ['ovo'],
                   'clf__kernel': ['rbf']}]
                
    gs = GridSearchCV(estimator=pipe_svc, 
                      param_grid=param_grid, scoring='accuracy', 
                      cv=5, n_jobs=2, verbose=1)
                    
    gs = gs.fit(X_train, y_train)
    
    print(algoritmo)                
    print(gs.best_params_)
                    
    clf = gs.best_estimator_
    print (algoritmo)
    print(clf.score(X_train, y_train))
    print(clf.score(X_test, y_test))
    joblib.dump(clf, 'SVM_' + algoritmo +'.pkl')
    
    y_pred = clf.predict(X_test)
    
    cnf_matrix = confusion_matrix(y_test, y_pred)
    np.set_printoptions(precision=2)
    
    # Plot non-normalized confusion matrix
    plt.figure(figsize=(7,7))
    plot_confusion_matrix(cnf_matrix, classes=['0','1','2','3','4','5',
                                               '6','7','8','9','10','11','12',
                                               '13','14','15'
                                               ],
                          normalize=True,
                          title='Confusion matrix, without normalization')
    
    plt.show()
        
