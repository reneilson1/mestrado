function [testeDataR,LabelR,testeDataSVM,LabelsSVM] = BaseTeste(A,B,C,D,E,F,G,H)
    
% base de dados para a rede neural
    testeDataR = zeros(size(A,2),300*8);
    LabelR = zeros(9,300*8);
% base de dados para o SVM
    testeDataSVM = zeros(300*8,size(A,2));  
    LabelsSVM = zeros(300*8,1);

    j = 1;
    for i = 700:1000
        testeDataR(:,j) = A(i,:)'; 
        testeDataSVM(j,:) = A(i,:);
        LabelsSVM(j,1) = 1;
        LabelR(1,j) = 1;j = j+1;


        testeDataR(:,j) = B(i,:)'; 
        LabelR(2,j) = 1;
        testeDataSVM(j,:) = B(i,:);
        LabelsSVM(j,1) = 2; j = j+1;

        testeDataR(:,j) = C(i,:)'; 
        testeDataSVM(j,:) = C(i,:);
        LabelsSVM(j,1) = 3;
        LabelR(3,j) = 1;j = j+1;
        
        testeDataR(:,j) = D(i,:)'; 
        testeDataSVM(j,:) = D(i,:);
        LabelsSVM(j,1) = 4;
        LabelR(4,j) = 1;j = j+1;
        
        testeDataR(:,j) = E(i,:)';
        testeDataSVM(j,:) = E(i,:);
        LabelsSVM(j,1) = 5;
        LabelR(5,j) = 1;j = j+1;
        
        testeDataR(:,j) = F(i,:)';
        testeDataSVM(j,:) = F(i,:);
        LabelsSVM(j,1) = 6;
        LabelR(6,j) = 1;j = j+1;
        
        testeDataR(:,j) = G(i,:)';
        testeDataSVM(j,:) = G(i,:);
        LabelsSVM(j,1) = 7;
        LabelR(7,j) = 1; j = j+1;

        testeDataR(:,j) = H(i,:)';
        testeDataSVM(j,:) = H(i,:);
        LabelsSVM(j,1) = 8;
        LabelR(8,j) = 1; j = j+1;

%         testeDataR(:,j) = I(i,:)';
%         testeDataSVM(j,:) = I(i,:);
%         LabelsSVM(j,1) = 9;
%         LabelR(9,j) = 1; j = j+1;

    end
end