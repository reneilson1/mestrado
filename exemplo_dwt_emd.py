from pywt import wavedec
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from sklearn.tree import DecisionTreeClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
#from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
import math
from PyEMD import EMD
from Bucket import Bucket
from scipy import signal
from scipy.signal import hilbert
from sklearn import tree
import graphviz
from scipy.ndimage import filters
from sklearn.decomposition import FastICA, PCA
from scipy.signal import wiener
import pywt

b = Bucket(500,20,freq=60)
dados = b.createBucket()
#for i in range(dados.shape[0]):
#    df[i,:] = wiener(dados[i,:], mysize=10, noise=1)

df=filters.gaussian_filter1d(dados,2.75)

y = b.createLabelsSVM()

#Coeficientes DWT
coeffs = wavedec(dados,'db4', level=4)
cA4, cD4, cD3, cD2, cD1 = coeffs
del coeffs

#Filtro Nocth
fs = 10000.0  # Sample frequency (Hz)
f0 = 60.0  # Frequency to be removed from signal (Hz)
Q = 1.0  # Quality factor
w0 = f0/(fs/2)  # Normalized Frequency
# Design notch filter
b_, a_ = signal.iirnotch(w0, Q)
X__ = np.abs(signal.filtfilt(b_,a_,dados))

del fs, f0, Q, w0, b_, a_

Y = np.fft.fft(dados)
L = dados.shape[1]
P2 = np.abs(Y/L)
X_ = P2[:,1:np.round(L/2)+1]
del Y, L, P2

Y = np.fft.fft(X_)
L = X_.shape[1]
P2 = np.abs(Y/L)
X_2 = P2[:,1:np.round(L/2)+1]
del Y, L, P2

Y = np.fft.fft(X__)
L = X__.shape[1]
P2 = np.abs(Y/L)
X_3 = P2[:,1:np.round(L/2)+1]
del Y, L, P2

analytic_signal = hilbert(dados)
amplitude_envelope = np.abs(analytic_signal)

analytic_signal2 = hilbert(abs(df))
amplitude_envelope2 = np.abs(analytic_signal2)

Y = np.fft.fft(amplitude_envelope)
L = amplitude_envelope.shape[1]
P2 = np.abs(Y/L)
X_4 = P2[:,1:np.round(L/2)+1]
del Y, L, P2


sum_envelop = np.sum(amplitude_envelope, axis=1)
sum_fft = np.sum(X_, axis=1)
eng_fft = (X_**2).sum(axis=1)
sum_notch = np.sum(X__,axis=1)
    
sum_dwt = (cD1**2).sum(axis=1)
skew_data = stats.skew(dados,axis=1)
skew_dwt = stats.skew(cD1,axis=1)
std_data = np.std(dados, axis=1)

pca = PCA(n_components=9)
pcs= pca.fit_transform(dados)
pcs=np.sum(pcs, axis=1)
        
eng_data = (dados**2).sum(axis=1)
eng_fft = (X_**2).sum(axis=1)
eng_notch = (X__**2).sum(axis=1)
eng_hilbert = (amplitude_envelope**2).sum(axis=1)

cont_mx = 0
cont_mn = 0
cont1 = 0
cont2 = 0
max_ = np.zeros(dados.shape[0])
min_ = np.zeros(dados.shape[0])
up_ = np.zeros(dados.shape[0])
cont_max = np.zeros(dados.shape[0])
cont_min = np.zeros(dados.shape[0])

X_aux = X__
for i in range(X_aux.shape[0]):
    for j in range(1, X_aux.shape[1]-1):
        if ((X_aux[i, j] > X_aux[i,j+1]) and 
        (X_aux[i, j-1] < X_aux[i,j])):
            cont_mx = cont_mx + X_aux[i,j]
            cont1 = cont1 + 1
        if ((X_aux[i, j] < X_aux[i,j+1]) and 
        (X_aux[i, j-1] > X_aux[i,j])):
            cont_mn = cont_mn + X_aux[i,j]
            cont2 = cont2 + 1
        if (X_aux[i,j] > 1.1):
            up_[i] = up_[i] + 1
    max_[i] = cont_mx*cont1
    cont_max[i] = cont1
    min_[i] = cont_mn*cont2
    cont_min[i] = cont2
    cont_mx = 0
    cont_mn = 0
    cont1 = 0
    cont2 = 0

del X_aux

X_aux = X_3
cont_max2 = np.zeros(X_aux.shape[0])
cont_min2 = np.zeros(X_aux.shape[0])
max_2 = np.zeros(X_aux.shape[0])

for i in range(X_aux.shape[0]):
    for j in range(1, X_aux.shape[1]-1):
        if ((X_aux[i, j] > X_aux[i,j+1]) and 
        (X_aux[i, j-1] < X_aux[i,j])):
            cont_mx = cont_mx + X_aux[i,j]
            cont1 = cont1 + 1
        if ((X_aux[i, j] < X_aux[i,j+1]) and 
        (X_aux[i, j-1] > X_aux[i,j])):
            cont_mn = cont_mn + X_aux[i,j]
            cont2 = cont2 + 1
        if (X_aux[i,j] > 1.1):
            up_[i] = up_[i] + 1
    max_2[i] = cont_mx*cont1
    cont_max2[i] = cont1
    cont_min2[i] = cont2
    cont_mx = 0
    cont_mn = 0
    cont1 = 0
    cont2 = 0

del X_aux

X_aux = amplitude_envelope
cont_max3 = np.zeros(X_aux.shape[0])
cont_min3 = np.zeros(X_aux.shape[0])
max_3 = np.zeros(X_aux.shape[0])

for i in range(X_aux.shape[0]):
    for j in range(1, X_aux.shape[1]-1):
        if ((X_aux[i, j] > X_aux[i,j+1]) and 
        (X_aux[i, j-1] < X_aux[i,j])):
            cont_mx = cont_mx + X_aux[i,j]
            cont1 = cont1 + 1
        if ((X_aux[i, j] < X_aux[i,j+1]) and 
        (X_aux[i, j-1] > X_aux[i,j])):
            cont_mn = cont_mn + X_aux[i,j]
            cont2 = cont2 + 1
        if (X_aux[i,j] > 1.1):
            up_[i] = up_[i] + 1
    max_3[i] = cont_mx*cont1
    cont_max3[i] = cont1
    cont_min3[i] = cont2
    cont_mx = 0
    cont_mn = 0
    cont1 = 0
    cont2 = 0

del X_aux

qa=50
qb=5
aux=dados
q = np.zeros([aux.shape[0], 
              1+len(range(0, aux.shape[1]-qa, qb))])
q2 = np.zeros(aux.shape[0])
k = 0
for i in range(aux.shape[0]):
    for j in range(0, aux.shape[1]-qa,qb):
        q[i, k] = np.max(aux[i,j:j+qa])
        k = k+1
    k = 0

for i in range(aux.shape[0]):
    for j in range(q.shape[1]-1):
        if(q2[i] < abs(q[i,j] - q[i,j+1])):
            q2[i] = q[i,j] - q[i,j+1]
        elif (q2[i] < abs(q[i,j+1] - q[i,j])):
            q2[i] = q[i,j+1] - q[i,j]
        
del qa, qb, k, i, j

aux = np.std(amplitude_envelope, axis=1)*(np.max(amplitude_envelope, axis=1) -
              np.min(amplitude_envelope, axis=1))

aux2 = np.std(X_3, axis=1)*(np.max(X_3, axis=1) -
              np.mean(X_3, axis=1))

aux3 = np.std(X__, axis=1)*(np.max(X__, axis=1) -
              np.min(X__, axis=1))*np.mean(X__, axis=1)

data = pywt.threshold(amplitude_envelope,1)

X = np.array([#np.sum(X_[:,1:3], axis=1),
              #np.max(q, axis=1)-
              #np.min(q[:,:85],axis=1),
              np.sum((df**4), axis=1),
              np.sum(((dados-df)**2), axis=1),
              np.sum(X__, axis=1),
              np.std(X__, axis=1),
              (1-np.min([np.max((q[:,:36]), axis=1),
                  np.max((q[:,41:60]), axis=1),
                  np.max((q[:,61:]), axis=1)], axis=0)),
              aux,
              #aux2,
              #aux3,
              #np.max((X_4-X_3)**2, axis=1),
              #np.max(abs(cD1),axis=1)-np.mean(abs(cD1),axis=1),
              #np.std(abs(amplitude_envelope-X__), axis=1),
              #sum_fft,
              #cont_max+cont_min,
              #np.sum(X_3[:,10:], axis=1),
              eng_data,
              np.sum(df,axis=1),
              np.mean(X_[:,:15], axis=1),
              np.mean(X_2[:,:15], axis=1),
              #cont_max2+cont_min2,
              #cont_max3+cont_min3,
              np.var(amplitude_envelope, axis=1),
              np.mean(amplitude_envelope, axis=1),
              np.max(X_[:,5:], axis=1)- np.mean(X_[:,5:], axis=1),
              ]).T

X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)

clf = DecisionTreeClassifier(random_state=1, criterion='entropy',
                             )
clf.fit(X_train, y_train)

print(clf.score(X_test, y_test))
print(clf.score(X_train, y_train))

######################EXTRAS####################################
y_pred = clf.predict(X_test)

cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['0','1','2','3','4','5',
                                           '6','7','8','9'],
                      normalize=True,
                      title='Confusion matrix, without normalization')

plt.show()
clf.feature_importances_


dot_data = tree.export_graphviz(clf, out_file=None, 
                                class_names=['0','1','2','3','4','5',
                                             '6','7','8','9'],
                                filled=True, rounded=True,  
                         special_characters=True)
graph = graphviz.Source(dot_data) 
graph.render('arvore')