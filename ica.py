import numpy as np
from PyEMD import EMD
from Bucket import Bucket
from sklearn.decomposition import FastICA
import matplotlib.pyplot as plt
import pywt
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import train_test_split
from scipy.ndimage import filters

b = Bucket(500,20,freq=60)

dados = b.createBucket()
df=filters.gaussian_filter1d(dados,2.75)
y = b.createLabelsSVM()

emd = EMD()

imf0 = np.zeros(dados.shape)
imf1 = np.zeros(dados.shape)
imf2 = np.zeros(dados.shape)
imf3 = np.zeros(dados.shape)
imf4 = np.zeros(dados.shape)
imf5 = np.zeros(dados.shape)
imf6 = np.zeros(dados.shape)

for i in range(dados.shape[0]):
    IMFS = emd(dados[i])
    imf0[i] = IMFS[0]
    imf1[i] = IMFS[1]
    imf2[i] = IMFS[2]
    if IMFS.shape[0] >3:
        imf3[i] = IMFS[3]
    if IMFS.shape[0] >4:
        imf4[i] = IMFS[4]
    if IMFS.shape[0] >5:
        imf5[i] = IMFS[5]
    if IMFS.shape[0] >6:
        imf6[i] = IMFS[6]
        
S = np.array([dados, imf0, imf1, imf2, imf3, imf4, imf5, imf6])
del imf0, imf1, imf2, imf3, imf4, imf5, IMFS, i, emd, imf6

data = pywt.threshold(dados,1, mode='less')

aux = np.array([S[1], S[2], S[3], S[3]+S[4]+S[5]+S[6]+S[7]])
aux = aux/np.linalg.norm(aux)

ica = FastICA(n_components=2)

X = np.zeros(dados.shape)

for i in range(dados.shape[0]):
    S_ = ica.fit_transform(aux[:,i,:].T)
    A_ = ica.mixing_
    #X[i] = S_[:,0]+(S[3]+S[4]+S[5]+S[6]+S[7])[i,:].T
    X[i] = S_[:,0]/max(S_[:,0])

#plt.plot(S_[:,0]+(S[3]+S[4]+S[5]+S[6]+S[7])[i,:].T)

X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)

clf = DecisionTreeClassifier(random_state=1, criterion='entropy')
clf.fit(X_train, y_train)

print(clf.score(X_test, y_test))
print(clf.score(X_train, y_train))