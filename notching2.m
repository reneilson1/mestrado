function [n, n_f, n_w, n_n] = notching2(N, ruido)
    t0 = 0:0.0001:3/60;
    t2 = 0:0.0001:57/60;
    
    %Senoide pura
    h0 = sin(2*pi*t0*60);
    
    %Inicializando vari�veis de retorno
    n = cell(1);
    n_w = cell(1);
    n_f = cell(1);
    n_n = cell(1);
    
    %Par�metros do filtro notch
    wo = 60/(10*size(t0,2)); bw=wo/35;
    [b, a] = iirnotch(wo, bw);

    for j = 1:N
        %Onde come�a o dist�rbio
        r1 = round(size(h0,2)/(4+round(rand(1)*8)));
        %Amplitude do dist�rbio
        alpha = 0.001*(10+round(rand(1)*190));
        %Inicializando vetor que conter� os dist�rbios
        h1 = zeros(size(h0));
        %Existir�o no m�ximo doze dist�rbios notching seguidos
        for k = 1:12
            if r1*(k) < size(t0,2)
                h1(:,r1*(k)) = 1;
            end
        end
        
        %Vari�vel com sen�ide e notching
        aux = h0+alpha*h1;
        if (ruido ~= 0)
            aux = add_awgn_noise(aux, ruido);
        end
        n{j,1}=aux;

       %Gerando sinal com notch
        hf = filter(b, a, [sin(2*pi*t2*60) aux]);
        n_n{j,1} = hf(:,size(t2,2)+1:size(hf,2));
        
        %Gerando sinal com FFT
        Y = fft(aux);
        L = size(t0,2);
        P2 = abs(Y/L);
        P1 = P2(1:round(L/2)+1);
        P1(2:end-1) = 2*P1(2:end-1);
        
        n_f{j,1} = P1;
        
        %Gerando sinal com Wavelet
        [C, L] =  wavedec(aux, 4, 'db4');
        d1 = appcoef(C, L, 'db4', 1);
        
        n_w{j,1} = d1;
    end
    %Transformando em matriz
    n = cell2mat(n);
    n_w = cell2mat(n_w);
    n_f = cell2mat(n_f);
    n_n = cell2mat(n_n);
end      