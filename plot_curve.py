from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

ml = ['SVM', 'MLP']
algoritmo = ['n', 'f', 'w', '']
ruido = [10, 20, 30, 50, 0]
nome_ax= [0,100,200,350]

mean_ac = np.zeros([4,8])
sd_ac = np.zeros([4,8])


x = np.zeros(32)
y = np.zeros(32)
z = np.zeros(32)
l=0
ac_all = np.zeros([4,8,50])

for k in range(4):
    for i in range(4):
        for j in range(2):
            ac = np.load('Accuracy_'+ml[j]+'_'+str(ruido[k])+'db_' + algoritmo[i] + '.npy')
            
            mean_ac[k,j*4+i] = np.mean(ac)
            sd_ac[k,j*4+i] = np.std(ac)
            z[l] = np.mean(ac)
            l+=1
            ac_all[k,j*4+i,:]=ac

ruido[3] = 100
for i in range(32):
    x[i] = i%8
    y[i] = nome_ax[i/8]

fr = friedman_aligned_ranks_test(ac_all[0,0,:], ac_all[0,1,:], ac_all[0,2,:],ac_all[0,3,:], 
                                 ac_all[0,4,:], ac_all[0,5,:], ac_all[0,6,:], ac_all[0,7,:])
    
for i in range(4):
    plt.figure()
    plt.boxplot(ac_all[i,:,:].T)
    plt.xticks([1,2,3,4,5,6,7,8], ['SN','MN','SF','MF','SW', 'MW', 'S','M'], rotation='vertical')
    plt.savefig(str(ruido[i])+'dB.jpg')

fig = plt.figure()
ax = fig.gca(projection='3d')
#ax.scatter(x, y, z, c='r', marker='o')
#ax.plot_trisurf(x, y, z, cmap=cm.coolwarm, linewidth=0.1, antialiased=True)
a=ax.get_yticks().tolist()
ax.set_yticklabels(['20dB','','30dB','', '50dB', '','','Noiseless'])
ax.set_xticklabels(['SN','MN','SF','MF','SW', 'MW', 'S','M'])
ax.view_init(30)
#plt.savefig('curva.png')
plt.show()

