import numpy as np

def labelsRNA(N):
    label = np.zeros([8,N])
    j = 0
    for i in range(N):
        label[j,i] = 1
        j = (j + 1)%8
    return label

def labelsSVM(N):
    label = np.zeros(N)
    for i in range(N):
        label[i] = i%8
    return label