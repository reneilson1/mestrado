m_RNA = mean(perRNA);
m_RNA_FFT = mean(perRNA_f);
m_RNA_Wav = mean(perRNA_w);
m_RNA_Notch = mean(perRNA_n);
m_SVM = mean(perSVM);
m_SVM_FFT = mean(perSVM_f);
m_SVM_Notch = mean(perSVM_n);
m_SVM_Wav = mean(perSVM_w);

s_RNA = std(perRNA);
s_RNA_FFT = std(perRNA_f);
s_RNA_Wav = std(perRNA_w);
s_RNA_Notch = std(perRNA_n);
s_SVM = std(perSVM);
s_SVM_FFT = std(perSVM_f);
s_SVM_Notch = std(perSVM_n);
s_SVM_Wav = std(perSVM_w);

mT_RNA = mean(perTRNA);
mT_RNA_FFT = mean(perTRNA_f);
mT_RNA_Wav = mean(perTRNA_w);
mT_RNA_Notch = mean(perTRNA_n);
mT_SVM = mean(perTSVM);
mT_SVM_FFT = mean(perTSVM_f);
mT_SVM_Notch = mean(perTSVM_n);
mT_SVM_Wav = mean(perTSVM_w);

sT_RNA = std(perTRNA);
sT_RNA_FFT = std(perTRNA_f);
sT_RNA_Wav = std(perTRNA_w);
sT_RNA_Notch = std(perTRNA_n);
sT_SVM = std(perTSVM);
sT_SVM_FFT = std(perTSVM_f);
sT_SVM_Notch = std(perTSVM_n);
sT_SVM_Wav = std(perTSVM_w);

%%%%%%%%%%%%%%%%%%%%%%%CALCULO TESTE KOLMOGOROV-SMIRNOV%%%%%%%%%%%%%%%%%%%%
h_RNA=1:8;
h_RNA_FFT=1:8;
h_RNA_Notch=1:8;
h_RNA_Wav=1:8;

h_SVM=1:8;
h_SVM_FFT=1:8;
h_SVM_Notch=1:8;
h_SVM_Wav=1:8;

p_RNA(i)= 1:8;
p_RNA_Wav(i)= 1:8;
p_RNA_FFT(i)= 1:8;
p_RNA_Notch(i)= 1:8;

p_SVM(i)= 1:8;
p_SVM_FFT(i)= 1:8;
p_SVM_Notch(i)= 1:8;
p_SVM_Wav(i)= 1:8;

for i=1:8
    [h_RNA(i), p_RNA(i)]=kstest(perRNA(:,i));
    [h_RNA_FFT(i), p_RNA_FFT(i)]=kstest(perRNA_f(:,i));
    [h_RNA_Notch(i), p_RNA_Notch(i)]=kstest(perRNA_n(:,i));
    [h_RNA_Wav(i), p_RNA_Wav(i)]=kstest(perRNA_w(:,i));
    
    [h_SVM(i), p_SVM(i)]=kstest(perSVM(:,i));
    [h_SVM_FFT(i), p_SVM_FFT(i)]=kstest(perSVM_f(:,i));
    [h_SVM_Notch(i), p_SVM_Notch(i)]=kstest(perSVM_n(:,i));
    [h_SVM_Wav(i), p_SVM_Wav(i)]=kstest(perSVM_w(:,i));
end

[hT_RNA, pRNA]=kstest(perTRNA);
[hT_RNA_FFT, pFFT]=kstest(perTRNA_f);
[hT_RNA_Notch, pNotch]=kstest(perTRNA_n);
[hT_RNA_Wav, pWav]=kstest(perTRNA_w);

[hT_SVM, pSVM]=kstest(perTSVM);
[hT_SVM_FFT, psF]=kstest(perTSVM_f);
[hT_SVM_Notch, psN]=kstest(perTSVM_n);
[hT_SVM_Wav, psW]=kstest(perTSVM_w);

%%%%%%%%%%%%%%%%%%%%%TESTE DE FRIEDMAN%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 [P, ANOVATAB, STATS] = friedman([perTSVM' perTRNA' perTSVM_w' ...
     perTRNA_w' perTSVM_f' perTRNA_f' perTSVM_n' perTRNA_n']);
 
  multcompare(STATS);
 
 P_ = 1:8;
 ANOVATAB_ = cell(1);
 STATS_ = cell(1);
 
%  for i=1:8
%      [P_(i), ANOVATAB_{i}, STATS_{i}] = friedman([perSVM_w(:,i) ...
%          perRNA_w(:,i) perSVM_f(:,i) perRNA_f(:,i) perSVM_n(:,i) ...
%          perRNA_n(:,i) perSVM(:,i) perRNA(:,i)],1);
%      
%       multcompare(STATS_{i});
%  end