import numpy as np
import matplotlib.pyplot as plt

ml = ['SVM', 'MLP']
algoritmo = ['n', 'f', 'w', '']
ruido = [0, 20, 30, 50]
mean_ac = np.zeros(4)
sd_ac = np.zeros(4)
mean_ac_each = np.zeros([4,8])
sd_ac_each = np.zeros([4,8])


for k in range(4):
    print(ruido[k])
    for i in range(4):
        ac = np.load('Accuracy_'+ml[0]+'_'+str(ruido[k])+'db_' + algoritmo[i] + '.npy')
        ac_each = np.load('Accuracy_'+ml[0]+'_'+str(ruido[k])+'dbEach_' + algoritmo[i]+ '.npy')
        
        mean_ac[i] = np.mean(ac)
        sd_ac[i] = np.std(ac)
        
        
        for j in range(8):
            mean_ac_each[i,j] = np.mean(ac_each[j])
            sd_ac_each[i,j] = np.std(ac_each[j])
            #print (str(j) + algoritmo[i] + ' - ' + str(mean_ac_each[i,j]))
            #print (str(j) + algoritmo[i] + ' - ' + str(sd_ac_each[i,j]))
    np.savetxt ('Media_Total'+ml[0]+ str(ruido[k]), mean_ac, delimiter=',')
    np.savetxt ('std_Total'+ml[0] + str(ruido[k]), sd_ac, delimiter=',')
    np.savetxt('Media_'+ml[0] + str(ruido[k]) + '.csv', mean_ac_each, delimiter=',')
    np.savetxt('Std_'+ml[0] + str(ruido[k]) + '.csv', sd_ac_each, delimiter=',')
    
    
ac = np.zeros([8, 50]);
for k in range(2):
    print(ml[k])
    for i in range(4):
        ac[k*4+i,:] = np.load('Accuracy_'+ml[k]+'_'+str(ruido[0])+'db_' + algoritmo[i] + '.npy')