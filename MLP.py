import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.externals import joblib
from numpy import genfromtxt
from labels import labelsRNA
from scipy import signal
import pywt
import scipy

def getArquivo(ruido, algoritmo,i):
    if algoritmo in ['w','f','n']:
        if(ruido != 0):
            X = genfromtxt(str(ruido)+'dB/' + algoritmo + '/conjunto_ruido_'+str(ruido)+'_' + str(i) + '_' + algoritmo + '.csv', delimiter=',')
        else:
            X = genfromtxt(str(ruido)+'dB/' + algoritmo + '/conjunto_ruido' + str(i) + '_' + algoritmo + '.csv', delimiter=',')
    elif (algoritmo == ''):
        if(ruido != 0):
            X = genfromtxt(str(ruido)+'dB/conjunto_ruido_'+str(ruido)+'_' + str(i) + '.csv', delimiter=',')
        else:
            X = genfromtxt(str(ruido)+'dB/conjunto_ruido' + str(i) + '.csv', delimiter=',')
    else:
        X = 0;
    return X;

#Variaveis uteis
ruidos = [20]
n=50
algoritmos = ['w', 'n', 'f', '']

#Variaveis de Armazenamento de Metricas
acuracy = np.zeros(n)
acuracy_individual = np.zeros([8,n])
f1 = np.zeros(n)

#Criacao de labels (todos sao iguais para o MLP devido a quantidade 
# de elementos no vetor de entrada 4000)
y = labelsRNA(8000).T;

for ruido in ruidos:
    for algoritmo in algoritmos:
        #Classificador armazenado em arquivo (salvo apos gridSearch)
        classifier = joblib.load('MLP_' + algoritmo + '.pkl') 
        
        print(algoritmo)
        print(classifier.get_params())
        for i in range(1):
            #Ler o arquivo
            #X = getArquivo(ruido, '', i+1)
            X_aux=np.load('dados20.npy')
            if(algoritmo == 'f'):
                #FFT 
                Y = np.fft.fft(X_aux)
                L = X_aux.shape[1]
                P2 = np.abs(Y/L)
                X = P2[:,1:np.round(L/2)+1]
                del Y, L, P2
            elif (algoritmo == 'w'):
                #Wavelet 
                cA, cD = pywt.dwt(X_aux, 'db4')
                X = cA
                del cA
            elif (algoritmo == 'n'):
                #Filtro Nocth
                fs = 10000.0  # Sample frequency (Hz)
                f0 = 60.0  # Frequency to be removed from signal (Hz)
                Q = 30.0  # Quality factor
                w0 = f0/(fs/2)  # Normalized Frequency
                # Design notch filter
                b, a = signal.iirnotch(w0, Q)
                X = np.abs(signal.filtfilt(b,a,X_aux))
                del fs, f0, Q, w0,b,a
            
            if algoritmo == 'w':
                aux = np.zeros([32,X.shape[0]])
                aux[0] = np.mean(X,axis=1)
                aux[1] = np.std(X, axis=1)
                aux[2] = np.max(X, axis=1)
                aux[3] = np.min(X, axis=1)
                aux[4] = np.sum(X, axis=1)
                aux[5] = scipy.stats.kurtosis(X,axis=1)
                aux[6] = 0#scipy.stats.gmean(X,axis=1)
                aux[7] = 0#scipy.stats.hmean(np.abs(X),axis=1)
                aux[8] = np.sqrt(np.mean(X**2))
                aux[9] = scipy.stats.moment(X,axis=1)
                aux[10] = scipy.stats.skew(X,axis=1)
                aux[11] = scipy.stats.tmean(X,axis=1)
                aux[12] = scipy.stats.tvar(X,axis=1)
                aux[13] = scipy.stats.tstd(X,axis=1)
                aux[14] = np.mean(cD,axis=1)
                aux[15] = np.std(cD, axis=1)
                aux[16] = np.max(cD, axis=1)
                aux[17] = np.min(cD, axis=1)
                aux[18] = np.sum(cD, axis=1)
                aux[19] = scipy.stats.kurtosis(cD,axis=1)
                aux[20] = np.sqrt(np.mean(cD**2))
                aux[21] = scipy.stats.moment(cD,axis=1)
                aux[22] = scipy.stats.skew(cD,axis=1)
                aux[23] = scipy.stats.tmean(cD,axis=1)
                aux[24] = scipy.stats.tvar(cD,axis=1)
                aux[25] = scipy.stats.tstd(cD,axis=1)
                aux[26] = 0#scipy.stats.entropy(X.T)
                aux[27] = 0#scipy.stats.entropy(cD.T)
                aux[28] = np.mean(scipy.signal.welch(X,axis=1)[1], axis=1)
                aux[29] = np.mean(scipy.signal.welch(cD,axis=1)[1], axis=1)
                aux[30] = np.sum(np.abs(X)**2, axis=1)
                aux[31] = np.sum(np.abs(cD)**2, axis=1)
            else:
                aux = np.zeros([14,X.shape[0]])
                aux[0] = np.mean(X,axis=1)
                aux[1] = np.std(X, axis=1)
                aux[2] = np.max(X, axis=1)
                aux[3] = np.min(X, axis=1)
                aux[4] = np.sum(X, axis=1)
                aux[5] = scipy.stats.kurtosis(X,axis=1)
                aux[6] = np.sqrt(np.mean(X**2))
                aux[7] = scipy.stats.moment(X,axis=1)
                aux[8] = scipy.stats.skew(X,axis=1)
                aux[9] = scipy.stats.tmean(X,axis=1)
                aux[10] = scipy.stats.tvar(X,axis=1)
                aux[11] = scipy.stats.tstd(X,axis=1)
                aux[12] = np.mean(scipy.signal.welch(X,axis=1)[1], axis=1)
                aux[13] = np.sum(np.abs(X)**2, axis=1)
                
            X = aux.T            
            y_pred = classifier.predict(X)
            
            res = y_pred + y
            
            for j in range(8):
                acuracy_individual[j,i] = (np.argwhere(res[:,j]==2).shape[0])/(
                        float(y.shape[0]/8))
                
            
            acuracy[i] = accuracy_score(y, y_pred)
            f1[i] = f1_score(y, y_pred, average='macro')
            
            #Imprime resultado
            #print(classifier.score(X, y))
            #print(f1[i])
        
        np.save('Accuracy_MLP_'+str(ruido)+'db_' + algoritmo, acuracy)
        np.save('Accuracy_MLP_'+str(ruido)+'dbEach_' + algoritmo, acuracy_individual)
        print(algoritmo + str(np.mean(acuracy)) + "+/-" + str(np.std(acuracy)))
        #print(str(np.mean(f1)) + "+/-" + str(np.std(f1)))


