from sklearn.externals import joblib
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from convert import convert
from sklearn.metrics import confusion_matrix
from Bucket import Bucket

b = Bucket(1000,0,freq=60)
b1 = Bucket(1000,10,freq=60)
b2 = Bucket(1000,20,freq=60)
b3 = Bucket(1000,30,freq=60)
b4 = Bucket(1000,50,freq=60)

dados = b.createBucket()
dados1 = b1.createBucket()
dados2 = b2.createBucket()
dados3 = b3.createBucket()
dados4 = b4.createBucket()

y = b.createLabelsSVM()

classifier = joblib.load('RF_Ruido.pkl') 

X = convert(dados)
X1 = convert(dados1)
X2 = convert(dados2)
X3 = convert(dados3)
X4 = convert(dados4)


y_pred = classifier.predict(X)
y_pred1 = classifier.predict(X1)
y_pred2 = classifier.predict(X2)
y_pred3 = classifier.predict(X3)
y_pred4 = classifier.predict(X4)

cnf_matrix = np.zeros([10,10,5])

cnf_matrix[:,:,0] = confusion_matrix(y, y_pred)
cnf_matrix[:,:,1] = confusion_matrix(y, y_pred1)
cnf_matrix[:,:,2] = confusion_matrix(y, y_pred2)
cnf_matrix[:,:,3] = confusion_matrix(y, y_pred3)
cnf_matrix[:,:,4] = confusion_matrix(y, y_pred4)

del X, X1, X2, X3, X4, dados, dados1, dados2, dados3, dados4
del y_pred, y_pred1, y_pred2, y_pred3, y_pred4
del b, b1, b2, b3, b4

#algoritmo = ['n', 'f', 'w', '']
#ruido = [10, 20, 30, 50, 0]
#nome_ax= [0,100,200,350]
#
#mean_ac = np.zeros([4,8])
#sd_ac = np.zeros([4,8])
#
#
#x = np.zeros(32)
#y = np.zeros(32)
#z = np.zeros(32)
#l=0
#ac_all = np.zeros([4,8,50])
#
#for k in range(4):
#    for i in range(4):
#        for j in range(2):
#            ac = np.load('Accuracy_'+ml[j]+'_'+str(ruido[k])+'db_' + algoritmo[i] + '.npy')
#            
#            mean_ac[k,j*4+i] = np.mean(ac)
#            sd_ac[k,j*4+i] = np.std(ac)
#            z[l] = np.mean(ac)
#            l+=1
#            ac_all[k,j*4+i,:]=ac
#
#ruido[3] = 100
#for i in range(32):
#    x[i] = i%8
#    y[i] = nome_ax[i/8]
#
#fr = friedman_aligned_ranks_test(ac_all[0,0,:], ac_all[0,1,:], ac_all[0,2,:],ac_all[0,3,:], 
#                                 ac_all[0,4,:], ac_all[0,5,:], ac_all[0,6,:], ac_all[0,7,:])
#    
#for i in range(5):
#    plt.figure()
#    plt.boxplot(ac_all[i,:,:].T)
#    plt.xticks([1,2,3,4,5,6,7,8], ['SN','MN','SF','MF','SW', 'MW', 'S','M'], rotation='vertical')
#    plt.savefig(str(ruido[i])+'dB.jpg')

for i in range(10):
    for j in range(10):
        for k in range(5):
            cnf_matrix[i,j,k] = cnf_matrix[i,j,k]/1000

x = np.zeros(50)
y = np.zeros(50)
z = np.zeros(50)
l = 0

for i in range(1,6):
    for j in range(10):
        z[l] = cnf_matrix[j,j,i%5]/1000
        l += 1

for i in range(50):
    x[i] = i%10
    y[i] = i/10

fig = plt.figure()
ax = fig.gca(projection='3d')
#x,y = np.meshgrid(x, y)
ax.plot_trisurf(y,x,z)
#ax.scatter(x, y, z, c='r', marker='o')
#ax.plot_trisurf(x, y, z, cmap=cm.coolwarm, linewidth=0.1, antialiased=True)
a=ax.get_yticks().tolist()
ax.set_xticklabels(['S','I','Sg','Sw', 'O', 'Flk','Hm','N', 'Spk', 'DC'])
ax.set_yticklabels(['10dB','20dB','30dB','50dB','Noiseless'])
ax.view_init(30)
#plt.savefig('curva.png')
plt.show()

