function [trainDataR,LabelR,trainDataSVM,LabelsSVM] = BaseDados(A,B,C,D,E,F,G,H,N)
    m = 8;
    % base de dados para a rede neural
    trainDataR = zeros(size(A,2),N*m);
    LabelR = zeros(m,N*m);
    % base de dados para o SVM
    trainDataSVM = zeros(N*m,size(A,2));  
    LabelsSVM = zeros(N*m,1);

    j = 1;
    for i = 1:N
        trainDataR(:,j) = A(i,:)'; 
        trainDataSVM(j,:) = A(i,:);
        LabelsSVM(j,1) = 1;
        LabelR(1,j) = 1;j = j+1;


        trainDataR(:,j) = B(i,:)'; 
        LabelR(2,j) = 1;
        trainDataSVM(j,:) = B(i,:);
        LabelsSVM(j,1) = 2; j = j+1;

        trainDataR(:,j) = C(i,:)'; 
        trainDataSVM(j,:) = C(i,:);
        LabelsSVM(j,1) = 3;
        LabelR(3,j) = 1;j = j+1;
        
        trainDataR(:,j) = D(i,:)'; 
        trainDataSVM(j,:) = D(i,:);
        LabelsSVM(j,1) = 4;
        LabelR(4,j) = 1;j = j+1;
        
        trainDataR(:,j) = E(i,:)';
        trainDataSVM(j,:) = E(i,:);
        LabelsSVM(j,1) = 5;
        LabelR(5,j) = 1;j = j+1;
        
        trainDataR(:,j) = F(i,:)';
        trainDataSVM(j,:) = F(i,:);
        LabelsSVM(j,1) = 6;
        LabelR(6,j) = 1;j = j+1;
        
        trainDataR(:,j) = G(i,:)';
        trainDataSVM(j,:) = G(i,:);
        LabelsSVM(j,1) = 7;
        LabelR(7,j) = 1; j = j+1;

        trainDataR(:,j) = H(i,:)';
        trainDataSVM(j,:) = H(i,:);
        LabelsSVM(j,1) = 8;
        LabelR(8,j) = 1; j = j+1;

%         trainDataR(:,j) = I(i,:)';
%         trainDataSVM(j,:) = I(i,:);
%         LabelsSVM(j,1) = 9;
%         LabelR(9,j) = 1; j = j+1;

    end
end