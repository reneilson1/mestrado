# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 17:08:13 2017

@author: Administrador
"""
from Bucket import Bucket
import numpy as np

#Criando Benchmark 1
b = Bucket(1000,0,freq=60)
X = b.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b1', X)

#Criando Benchmark 2
b1 = Bucket(1000,10,60)
X = b1.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b2_1', X)
b2 = Bucket(1000,20,60)
X = b2.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b2_2', X)
b3 = Bucket(1000,30,60)
X = b3.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b2_3', X)
b4 = Bucket(1000,50,60)
X = b4.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b2_4', X)


#Criando Benchmark 3
b = Bucket(1000,0,50)
X = b.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b3', X)

#Criando Benchmark 4
b1 = Bucket(1000,10,50)
X = b1.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b4_1', X)
b2 = Bucket(1000,20,50)
X = b2.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b4_2', X)
b3 = Bucket(1000,30,50)
X = b3.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b4_3', X)
b4 = Bucket(1000,50,50)
X = b4.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b4_4', X)


#Criando Benchmark 5
b = Bucket(1000,0,60,False)
X = b.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b5', X)

#Criando Benchmark 6
b1 = Bucket(1000,10,60, False)
X = b1.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b6_1', X)
b2 = Bucket(1000,20,60, False)
X = b2.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b6_2', X)
b3 = Bucket(1000,30,60, False)
X = b3.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b6_3', X)
b4 = Bucket(1000,50,60, False)
X = b4.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b6_4', X)


#Criando Benchmark 7
b = Bucket(1000,0,50,False)
X = b.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b7', X)

#Criando Benchmark 8
b1 = Bucket(1000,10,50, False)
X = b1.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b8_1', X)
b2 = Bucket(1000,20,50, False)
X = b2.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b8_2', X)
b3 = Bucket(1000,30,50, False)
X = b3.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b8_3', X)
b4 = Bucket(1000,50,50, False)
X = b4.createBucket()
np.save('/home/reneilson/Documents/Benchmarks/b8_4', X)


del X, b, b1, b2, b3