function [sg, sg_f, sg_w, sg_n] = sag2(N, ruido)
    t0 = 0:0.0001:3/60;
    t2 = 0:0.0001:57/60;
    
    %Senoide pura
    h0 = sin(2*pi*t0*60);
    
    %Inicializando vari�veis de retorno
    sg = cell(1);
    sg_w = cell(1);
    sg_f = cell(1);
    sg_n = cell(1);
    
    %Par�metros do filtro notch
    wo = 60/(10*size(t0,2)); bw=wo/35;
    [b, a] = iirnotch(wo, bw);

    for j = 1:N
        %Onde come�a e termina o afundamento de tens�o
        r1 = round(rand(1)*100);
        r2 = 100+round(rand(1)*350);
        %Amplitude do afundamento
        alpha = 0.01*(10+round(rand(1)*80));

        h1 = [h0(:,1:min(r1,r2)) h0(:,min(r1,r2)+1:max(r1,r2))*alpha h0(:,max(r1,r2)+1:size(h0,2))];
        
        %Vari�vel com sen�ide pura
        aux = h1;
        if (ruido ~= 0)
            aux = add_awgn_noise(aux, ruido);
        end
        sg{j,1} = aux;
        
        %Gerando sinal com notch
        hf = filter(b, a, [sin(2*pi*t2*60) aux]);
        sg_n{j,1} = hf(:,size(t2,2)+1:size(hf,2));
        
        %Gerando sinal com FFT
        Y = fft(aux);
        L = size(t0,2);
        P2 = abs(Y/L);
        P1 = P2(1:round(L/2)+1);
        P1(2:end-1) = 2*P1(2:end-1);
        
        sg_f{j,1} = P1;
        
        %Gerando sinal com Wavelet
        [C, L] =  wavedec(aux, 4, 'db4');
        d1 = appcoef(C, L, 'db4', 1);
        
        sg_w{j,1} = d1;
    end
    %Transformando em matriz
    sg = cell2mat(sg);
    sg_w = cell2mat(sg_w);
    sg_f = cell2mat(sg_f);
    sg_n = cell2mat(sg_n);
end   