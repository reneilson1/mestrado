from pywt import wavedec
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from sklearn.tree import DecisionTreeClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
#from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import FastICA, PCA
import math
from PyEMD import EMD
from Bucket import Bucket
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from scipy import signal
from scipy.signal import hilbert
from scipy.signal import medfilt
from sklearn import tree
import graphviz
from scipy.ndimage import filters

b = Bucket(100,0,freq=60)

dados = b.createBucket()

y = b.createLabelsSVM()

Y = np.fft.fft(dados)
L = dados.shape[1]
P2 = np.abs(Y/L)
X_fft = P2[:,1:np.round(L/2)+1]
X_fft = 100*X_fft[:,10:]
del Y, L, P2

sum_fft = np.sum(X_fft, axis=1)
eng_fft = (X_fft**2).sum(axis=1)
eng_data = (dados**2).sum(axis=1)

cont_mx = 0
cont_mn = 0
cont1 = 0
cont2 = 0
max_ = np.zeros(dados.shape[0])
min_ = np.zeros(dados.shape[0])
cont_max = np.zeros(dados.shape[0])
cont_min = np.zeros(dados.shape[0])
cont_max_dados = np.zeros(dados.shape[0])

for i in range(X_fft.shape[0]):
    for j in range(1, X_fft.shape[1]-1):
        if ((X_fft[i, j] > X_fft[i,j+1]) and 
            (X_fft[i, j-1] < X_fft[i,j])):
            cont_mx = cont_mx + (X_fft[i,j] - X_fft[i,j-1])
            cont1 = cont1 + 1
        if ((X_fft[i, j] < X_fft[i,j+1]) and 
            (X_fft[i, j-1] > X_fft[i,j])):
            cont_mn = cont_mn + X_fft[i,j]
            cont2 = cont2 + 1
    max_[i] = cont_mx*cont1
    cont_max[i] = cont1
    min_[i] = cont_mn*cont2
    cont_min[i] = cont2
    cont_mx = 0
    cont_mn = 0
    cont1 = 0
    cont2 = 0

max_aux = np.zeros(dados.shape[0])
for i in range(dados.shape[0]):
    for j in range(1, dados.shape[1]-1):
        if ((dados[i, j] > dados[i,j+1]) and 
            (dados[i, j-1] < dados[i,j])):
            max_aux[i] = max_aux[i] + 1
max_aux = max_aux-3

del i, j, cont1
del cont2, cont_mn, cont_mx

aux = np.zeros(dados.shape)
for i in range(dados.shape[0]):
    for j in range(dados.shape[1]):
        aux[i,j] = np.max(abs(dados[i,j:j+100%dados.shape[1]]))
        
aux2 = np.zeros(X_fft.shape)
for i in range(X_fft.shape[0]):
    for j in range(X_fft.shape[1]):
        aux2[i,j] = np.max(abs(X_fft[i,j:j+50%X_fft.shape[1]]))
        
tam = dados.shape[1]
std = np.std(np.array([np.mean(dados[:,0:tam/5], axis=1), 
              np.mean(dados[:,tam/5:(tam/5)*2], axis=1),
              np.mean(dados[:,(tam/5)*2:(tam/5)*3], axis=1), 
              np.mean(dados[:,(tam/5)*3:(tam/5)*4], axis=1), 
              np.mean(dados[:,(tam/5)*4:], axis=1)]).T,axis=1)

maxi = 4
std = np.zeros([maxi, dados.shape[0]])
for i in range(maxi):
    std[i,:] = np.mean(dados[:,i*(tam/maxi):(i+1)*(tam/maxi)],axis=1)

soma_fft = np.sum(X_fft, axis=1)
rms_dados = np.sqrt(np.mean(dados**2, axis=1))
dist_max_mean_dados = np.max(np.abs(dados), axis=1)-np.mean(dados, axis=1)
dist_min_mean_dados = -np.min(dados, axis=1)+np.mean(dados, axis=1)
dist_cont = cont_min-cont_max
min_env_dados = np.min(aux[:,:450], axis=1)
max_env_dados = np.max(aux[:,:450], axis=1)
dist_env_fft_mx = np.max(aux2, axis=1)-np.min(aux2[:,:200], axis=1)
dist_env_fft_mx_mean = np.max(aux2, axis=1)-np.mean(aux2, axis=1)
std_ = np.std(std.T,axis=1)

analytic_signal = hilbert(dados)
amplitude_envelope = np.abs(analytic_signal)

coeffs = wavedec(dados,'db4', level=4)
cA4, cD4, cD3, cD2, cD1 = coeffs
del coeffs

d = filters.gaussian_filter1d(dados,0.75)

X = np.array([soma_fft,
              max_**2,  
              rms_dados,
              std_,
              dist_max_mean_dados,
              dist_min_mean_dados,
              dist_cont,
              min_env_dados,
              max_env_dados,
              dist_env_fft_mx,
              dist_env_fft_mx_mean,
              max_aux,
              ]).T

# define os dados de treino e os dados de teste 70% e 30%
X_train, X_test, y_train, y_test = train_test_split(
         X, y, test_size=0.3, random_state=0)

clf = DecisionTreeClassifier(random_state=1, 
                             criterion='entropy',
                             max_depth=6)

clf.fit(X_train, y_train)

print(clf.score(X_test, y_test))

print(clf.score(X_train, y_train))

######################EXTRAS####################################
y_pred = clf.predict(X_test)

cnf_matrix = confusion_matrix(y_test, y_pred)
np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=['0','1','2','3','4','5','6',
                                           '7','8','9'],
                      normalize=True,
                      title='Confusion matrix, without normalization')

plt.show()

dot_data = tree.export_graphviz(clf, out_file=None, 
                                class_names=['0','1','2','3','4','5','6',
                                           '7','8','9'],
                                filled=True, rounded=True,  
                         special_characters=True)
graph = graphviz.Source(dot_data) 
graph.render('arvore')