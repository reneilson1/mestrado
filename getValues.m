perTRNA = cell(1);
perTRNA_f = cell(1);
perTRNA_w = cell(1);
perTRNA_n = cell(1);
perRNA = cell(1);
perRNA_f = cell(1);
perRNA_w = cell(1);
perRNA_n = cell(1);

for j=1:50
    loadfile = strcat('percentualRNA', mat2str(j), '.mat');
    load(loadfile);
    
    perTRNA{j} = cell2mat(RNAperT);
    perTRNA_f{j} = cell2mat(RNAperT_f);
    perTRNA_w{j} = cell2mat(RNAperT_w);
    perTRNA_n{j} = cell2mat(RNAperT_n);
    
    perRNA{j,1} = cell2mat(RNAper);
    perRNA_f{j,1} = cell2mat(RNAper_f);
    perRNA_w{j,1} = cell2mat(RNAper_w);
    perRNA_n{j,1} = cell2mat(RNAper_n);
    
end

perTRNA = cell2mat(perTRNA);
perTRNA_f = cell2mat(perTRNA_f);
perTRNA_w = cell2mat(perTRNA_w);
perTRNA_n = cell2mat(perTRNA_n);

perRNA = cell2mat(perRNA);
perRNA_f = cell2mat(perRNA_f);
perRNA_w = cell2mat(perRNA_w);
perRNA_n = cell2mat(perRNA_n);

perTRNA = 1-perTRNA;
perTRNA_f = 1-perTRNA_f;
perTRNA_w = 1-perTRNA_w;
perTRNA_n = 1-perTRNA_n;

perRNA = 1-perRNA;
perRNA_f = 1-perRNA_f;
perRNA_w = 1-perRNA_w;
perRNA_n = 1-perRNA_n;