t0 = 0:0.0001:3/60;
t1 = 0:0.0001:57/60;

f = zeros(1, size(t0,2));
h0 = sin(2*pi*t0*60);

wo = 60/(10*size(t0,2)); bw=wo/35;
[b, a] = iirnotch(wo, bw);

for j = 1:1000
    alpha = 0.001*(round(rand(1)*200));
    
    hm = round(1+rand(1)*9);
    h1 = alpha*sin(2*pi*t0*hm).*h0;
    
    hf = filter(b, a, [sin(2*pi*t1*60) h0+h1]);
    
    f = vertcat(f, hf(:,size(t1,2)+1:size(hf,2)));
end

    
            