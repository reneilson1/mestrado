from Bucket import Bucket
import numpy as np

b = Bucket(500, 0, 60, False)
b1 = Bucket(300, 10, 60, False)
b2 = Bucket(300, 20, 60, False)
b3 = Bucket(300, 30, 60, False)
b4 = Bucket(300, 50, 60, False)

dados0 = b.createBucket()
dados1 = b1.createBucket()
dados2 = b2.createBucket()
dados3 = b3.createBucket()
dados4 = b4.createBucket()

dados = np.concatenate([dados0, dados1, dados2, dados3, dados4])
dados = dados0
del dados0, dados1, dados2, dados3, dados4, b1, b2, b3, b4

y1 = b.createLabelsSVM()
y1_ = b.createLabels()

ys = np.concatenate([y1, y1, y1, y1, y1])
y = np.concatenate([y1_, y1_, y1_, y1_, y1_])
y = y1_
ys = y1
del y1, y1_

np.save('dados', dados)
np.save('targetSVM', ys)
np.save('target', y)

del dados, ys, y