function [i, i_f, i_w, i_n] = interrupcao2(N, ruido)
    t0 = 0:0.0001:3/60;
    t2 = 0:0.0001:57/60;
    
    %Senoide pura
    h0 = sin(2*pi*t0*60);
    
    %Inicializando vari�veis de retorno
    i = cell(1);
    i_w = cell(1);
    i_f = cell(1);
    i_n = cell(1);
    
    %Par�metros do filtro notch
    wo = 60/(10*size(t0,2)); bw=wo/35;
    [b, a] = iirnotch(wo, bw);

    for j = 1:N
        %Onde come�a e termina a oscila��o
        r1 = round(rand(1)*100);
        r2 = r1+100+round(rand(1)*(size(h0,2)-r1-100));

        h1 = [h0(:,1:min(r1,r2)) h0(:,min(r1,r2)+1:max(r1,r2))*0 h0(:,max(r1,r2)+1:size(h0,2))];
        
        %Vari�vel com sen�ide e interrup��o
        aux = h1;
        aux = awgn(aux, ruido);
        i{j,1} = aux;
        
        %Gerando sinal com notch
        hf = filter(b, a, [sin(2*pi*t2*60) aux]);
        i_n{j,1} = hf(:,size(t2,2)+1:size(hf,2));
        
        %Gerando sinal com FFT
        Y = fft(aux);
        L = size(t0,2);
        P2 = abs(Y/L);
        P1 = P2(1:round(L/2)+1);
        P1(2:end-1) = 2*P1(2:end-1);
        
        i_f{j,1} = P1;
        
        %Gerando sinal com Wavelet
        [C, L] =  wavedec(aux, 4, 'db4');
        d1 = appcoef(C, L, 'db4', 1);
        
        i_w{j,1} = d1;
    end
    %Transformando em matriz
    i = cell2mat(i);
    i_w = cell2mat(i_w);
    i_f = cell2mat(i_f);
    i_n = cell2mat(i_n);
end
    
            