perTSVM = cell(1);
perTSVM_f = cell(1);
perTSVM_w = cell(1);
perTSVM_n = cell(1);
perSVM = cell(1);
perSVM_f = cell(1);
perSVM_w = cell(1);
perSVM_n = cell(1);

for j=1:50
    loadfile = strcat('percentualSVM', mat2str(j), '.mat');
    load(loadfile);
    
    perTSVM{j} = cell2mat(SVMperT);
    perTSVM_f{j} = cell2mat(SVMperT_f);
    perTSVM_w{j} = cell2mat(SVMperT_w);
    perTSVM_n{j} = cell2mat(SVMperT_n);
    
    perSVM{j,1} = cell2mat(SVMper);
    perSVM_f{j,1} = cell2mat(SVMper_f);
    perSVM_w{j,1} = cell2mat(SVMper_w);
    perSVM_n{j,1} = cell2mat(SVMper_n);
    
end

perTSVM = cell2mat(perTSVM);
perTSVM_f = cell2mat(perTSVM_f);
perTSVM_w = cell2mat(perTSVM_w);
perTSVM_n = cell2mat(perTSVM_n);

perSVM = cell2mat(perSVM);
perSVM_f = cell2mat(perSVM_f);
perSVM_w = cell2mat(perSVM_w);
perSVM_n = cell2mat(perSVM_n);