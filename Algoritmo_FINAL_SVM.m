for k=23:50

    savefile = strcat('conjunto_ruido_50_', mat2str(k), '.mat')
    load(savefile);
    N = 5000;
%     ruido = 30;    
% % % % 
%     [nm, nm_f, nm_w, nm_n] = normal2(N, ruido);
%     [n, n_f, n_w, n_n] = notching2(N, ruido);
%     [o, o_f, o_w, o_n] = oscilacao2(N, ruido);
%     [sg, sg_f, sg_w, sg_n] = sag2(N, ruido);
%     [sw, sw_f, sw_w, sw_n] = swell2(N, ruido);
%     [i, i_f, i_w, i_n] = interrupcao2(N, ruido);[spk, spk_f, spk_w, spk_n] = spike2(N, ruido);
%     [h, h_f, h_w, h_n] = harmonica2(N, ruido);
% 
%     [trainDataR,LabelR,trainDataSVM,LabelsSVM] = BaseDados(nm,h,i,o,sg,sw,spk,n,N);
%     [trainDataR_f,LabelR_f,trainDataSVM_f,LabelsSVM_f] = BaseDados(nm_f,h_f,i_f,o_f,sg_f,sw_f,spk_f,n_f,N);
%     [trainDataR_n,LabelR_n,trainDataSVM_n,LabelsSVM_n] = BaseDados(nm_n,h_n,i_n,o_n,sg_n,sw_n,spk_n,n_n,N);
%     [trainDataR_w,LabelR_w,trainDataSVM_w,LabelsSVM_w] = BaseDados(nm_w,h_w,i_w,o_w,sg_w,sw_w,spk_w,n_w,N);
% 
    LabelsSVM = zeros(8*N,1);
    LabelsSVM_f = zeros(8*N,1);
    LabelsSVM_w = zeros(8*N,1);
    LabelsSVM_n = zeros(8*N,1);
    
    for i = 0:8*N-1
        LabelsSVM(i+1,1) = mod(i,8)+1;
        LabelsSVM_f(i+1,1) = mod(i,8)+1;
        LabelsSVM_w(i+1,1) = mod(i,8)+1;
        LabelsSVM_n(i+1,1) = mod(i,8)+1;
    end
% 
%     [RNAper,RNAperT] = KfoldRNA(trainDataSVM', LabelsSVM);
%     [RNAper_f,RNAperT_f] = KfoldRNA(trainDataSVM_f', LabelsSVM_f);
%     [RNAper_n,RNAperT_n] = KfoldRNA(trainDataSVM_n', LabelsSVM_n);
%     [RNAper_w,RNAperT_w] = KfoldRNA(trainDataSVM_w', LabelsSVM_w);

    [SVMper,SVMperT] = Kfold(trainDataSVM, LabelsSVM);
    [SVMper_f,SVMperT_f] = Kfold(trainDataSVM_f, LabelsSVM_f);
    [SVMper_w,SVMperT_w] = Kfold(trainDataSVM_w, LabelsSVM_w);
    [SVMper_n,SVMperT_n] = Kfold(trainDataSVM_n, LabelsSVM_n);

%     perSVM{j} = SVMper;
%     perSVM_f{j} = SVMper_f;
%     perSVM_w{j} = SVMper_w;
%     perSVM_n{j} = SVMper_n;
%     save(savefile, 'trainDataSVM','LabelsSVM', ...
%         'trainDataSVM_f','LabelsSVM_f', ...
%         'trainDataSVM_w','LabelsSVM_w', ...
%         'trainDataSVM_n','LabelsSVM_n');
    savefile2 = strcat('percentualSVM', mat2str(k), '.mat');
    save(savefile2, 'SVMper', 'SVMperT','SVMper_f', 'SVMperT_f', ...
        'SVMper_w', 'SVMperT_w', 'SVMper_n', 'SVMperT_n');
    
    clear;
end