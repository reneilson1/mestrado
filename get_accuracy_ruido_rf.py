from sklearn.externals import joblib
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from convert import convert
from sklearn.metrics import confusion_matrix
from Bucket import Bucket

classifier = joblib.load('RF_Ruido.pkl') 

quant = 50

ac_0 = np.zeros(quant)
ac_10 = np.zeros(quant)
ac_20 = np.zeros(quant)
ac_30 = np.zeros(quant)
ac_50 = np.zeros(quant)

cnf_matrix = np.zeros([50,10,10])
cnf_matrix1 = np.zeros([50,10,10])
cnf_matrix2 = np.zeros([50,10,10])
cnf_matrix3 = np.zeros([50,10,10])
cnf_matrix4 = np.zeros([50,10,10])
    
b = Bucket(200,0,freq=60)
y = b.createLabelsSVM()

for i in range(quant):
    
    dados = np.load('bench/bc0_' + str(i) + '.npy')
    dados1 = np.load('bench/bc10_' + str(i) + '.npy')
    dados2 = np.load('bench/bc20_' + str(i) + '.npy')
    dados3 = np.load('bench/bc30_' + str(i) + '.npy')
    dados4 = np.load('bench/bc50_' + str(i) + '.npy')
    
    X = convert(dados)
    X1 = convert(dados1)
    X2 = convert(dados2)
    X3 = convert(dados3)
    X4 = convert(dados4)
    
    
    ac_0[i] = classifier.score(X, y)
    ac_10[i] = classifier.score(X1, y)
    ac_20[i] = classifier.score(X2, y)
    ac_30[i] = classifier.score(X3, y)
    ac_50[i] = classifier.score(X4, y)
    
    y_pred = classifier.predict(X)
    y_pred1 = classifier.predict(X1)
    y_pred2 = classifier.predict(X2)
    y_pred3 = classifier.predict(X3)
    y_pred4 = classifier.predict(X4)
    
    cnf_matrix[i] = confusion_matrix(y, y_pred)
    cnf_matrix1[i] = confusion_matrix(y, y_pred1)
    cnf_matrix2[i] = confusion_matrix(y, y_pred2)
    cnf_matrix3[i] = confusion_matrix(y, y_pred3)
    cnf_matrix4[i] = confusion_matrix(y, y_pred4)
       
    del X, X1, X2, X3, X4, dados, dados1, dados2, dados3, dados4
    del y_pred, y_pred1, y_pred2, y_pred3, y_pred4
    
np.save('ac0', ac_0)
np.save('ac10', ac_10)
np.save('ac20', ac_20)
np.save('ac30', ac_30)
np.save('ac50', ac_50)

np.save('conf_0', cnf_matrix)
np.save('conf_10', cnf_matrix1)
np.save('conf_20', cnf_matrix2)
np.save('conf_30', cnf_matrix3)
np.save('conf_50', cnf_matrix4)